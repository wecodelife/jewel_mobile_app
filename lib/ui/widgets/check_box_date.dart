import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/build_clickable_icon.dart';
import 'package:jewel_app/ui/widgets/check_box_icons.dart';

class CheckBoxDate extends StatefulWidget {
  ValueChanged onChanged;
  String quest;
  bool decision;
  CheckBoxDate({this.onChanged,this.quest,this.decision});
  @override
  _CheckBoxDateState createState() => _CheckBoxDateState();
}

class _CheckBoxDateState extends State<CheckBoxDate> {
  bool yes=true;
  bool no=false;
  bool decision;
  bool yesClicked=true;
@override
  void initState() {
    decision=widget.decision;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: screenWidth(context,dividedBy: 14),top: screenHeight(context,dividedBy: 60)),
      child: Container(
        child: Padding(
          padding:  EdgeInsets.only(bottom:screenWidth(context,dividedBy: 20),top: screenWidth(context,dividedBy:60 ) ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            children: [
              // Padding(
              //   padding:  EdgeInsets.only(),
              //   child: Text(widget.quest,style: TextStyle(color: Colors.black,fontSize:14,fontFamily: 'Poppins'),),
              // ),
              Padding(
                padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 100)),
                child: Row(
                  children: [
                    GestureDetector(
                        onTap: (){
                          setState(() {
                            yes=!yes;
                            no=false;
                            decision=true;
                            widget.onChanged(decision);
                          });
                        },
                        child: BuildClickableIcon(checkBoxData: "Select Date",iconData:decision==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),

                    Padding(
                      padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 10)),
                      child: GestureDetector(
                          onTap: (){
                            setState(() {
                              no=!no;
                              yes=false;
                              decision=false;
                              widget.onChanged(decision);
                            });
                          },
                          child: BuildClickableIcon(checkBoxData: "Date Not Fixed",iconData:decision==false  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

      ),
    );
  }
}
