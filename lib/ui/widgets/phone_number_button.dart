import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';

class PhoneNumberButton extends StatefulWidget {
  String phoneNumber1;
  PhoneNumberButton({this.phoneNumber1});
  @override
  _PhoneNumberButtonState createState() => _PhoneNumberButtonState();
}

class _PhoneNumberButtonState extends State<PhoneNumberButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 40,),
            left: screenWidth(context, dividedBy: 15)),
        child: Container(
          height: screenHeight(context, dividedBy: 15),
          width: screenWidth(context, dividedBy: 3),
          decoration: BoxDecoration(

            color: Constants.kitGradients[3],
            borderRadius: BorderRadius.all(Radius.circular(15),),

          ),
          child: Row(
            mainAxisAlignment:MainAxisAlignment.spaceEvenly,
            children: [
              Center(
                child: Text("Add Ph", style: TextStyle(
                    color: Colors.white, fontFamily: 'Poppins', fontSize: 13),),
              ),
              Icon(Icons.add,size: 20,color: Colors.white,)
            ],
          ),
        ),
      ),
    );
  }
}









































































































































































