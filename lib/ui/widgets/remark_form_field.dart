import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/search_icons.dart';

class RemarkFormFieldBox extends StatefulWidget {
  String hintText;
  TextInputType keyboardType;
  TextEditingController textEditingController;
  RemarkFormFieldBox({this.hintText,this.textEditingController,this.keyboardType});
  @override
  _RemarkFormFieldBoxState createState() => _RemarkFormFieldBoxState();
}
class _RemarkFormFieldBoxState extends State<RemarkFormFieldBox> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 40,),left: screenWidth(context,dividedBy: 15)),
        child: Container(
          height: screenHeight(context,dividedBy: 5),
          width: screenWidth(context,dividedBy: 1.2),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black54),
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15),),

          ),
          child:
          TextField(controller:widget.textEditingController,
            keyboardType:widget.keyboardType,
            autofocus: false,
            style: TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 16),
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top:screenHeight(context,dividedBy: 700,),left: screenWidth(context,dividedBy: 15),),
                labelText: widget.hintText,
                labelStyle: TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 16),
                border: InputBorder.none

            ),
          ),
        ),
      ),
    );
  }
}
