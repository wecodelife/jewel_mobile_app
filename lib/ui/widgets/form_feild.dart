import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/search_icons.dart';

class FormFeildBox extends StatefulWidget {
  String hintText;
  TextEditingController textEditingController;
  FormFeildBox({this.hintText,this.textEditingController});
  @override
  _FormFeildBoxState createState() => _FormFeildBoxState();
}
class _FormFeildBoxState extends State<FormFeildBox> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 40,),left: screenWidth(context,dividedBy: 15)),
        child: Container(
          height: screenHeight(context,dividedBy: 15),
          width: screenWidth(context,dividedBy: 1.2),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black54),
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(15),),

          ),
          child: TextField(controller:widget.textEditingController,
            keyboardType:TextInputType.number,
            autofocus: false,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top:screenHeight(context,dividedBy: 700,),left: screenWidth(context,dividedBy: 15),),
                hintText: widget.hintText,
                hintStyle: TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 16
                ),
                border: InputBorder.none

            ),


          ),
        ),
      ),
    );
  }
}
