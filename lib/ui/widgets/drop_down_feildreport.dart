import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class DropReportPage extends StatefulWidget {
  ValueChanged onChanged;
  DropReportPage({this.onChanged});
  @override
  _DropReportPageState createState() => _DropReportPageState();
}

String dropdownvalue;


class _DropReportPageState extends State<DropReportPage> {
  @override
  void initState() {
    super.initState();
    setState(() {

    });
  }

  List<String> numbers = [
    "1-5",
    "5-10",
    "10-15",
    "15-20",
    "20-25",
    "25-30",
    "30-35",
    "35-40",
    "40-45",
    "45-50",
    "50-55",
    "55-60"

  ];

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: Container(
        width: screenWidth(context,dividedBy: 3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.grey[300],
        ),
        child: Padding(
          padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 10)),
          child: DropdownButton<String>(


            icon: Icon(Icons.keyboard_arrow_down_sharp,size: 30,color: Colors.black,),
            value: dropdownvalue,
            style: TextStyle(fontSize: 18, color: Colors.black,fontWeight: FontWeight.w900),
            items: numbers
                .map<DropdownMenuItem<String>>((String value) =>
                DropdownMenuItem<String>(value: value, child: Padding(
                  padding:  EdgeInsets.only(),
                  child: Text(value),
                )))
                .toList(),
            onChanged: (value) {
              widget.onChanged(value);
              setState(() {
                dropdownvalue = value;
              });
            },
            hint: Text(
              '1-5',
              style: TextStyle(fontSize: 16, color: Colors.black,fontWeight: FontWeight.w900),
            ),
          ),
        ),
      ),
    );
  }
}

