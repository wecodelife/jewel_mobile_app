import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jewel_app/src/utils/utils.dart';

class AdharImage extends StatefulWidget {
  ValueChanged onChanged;
  File aadharImage;
  AdharImage({this.aadharImage,this.onChanged});
  @override
  _AdharImageState createState() => _AdharImageState();
}

class _AdharImageState extends State<AdharImage> {

  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        widget.aadharImage = File(pickedFile.path);
        widget.onChanged(widget.aadharImage);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: (){
        getImage();

      },
      child: Padding(
        padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 20),top: screenHeight(context,dividedBy: 40)),
        child: ClipRRect(

          borderRadius: BorderRadius.circular(10),
          child: Container(
            height: screenHeight(context,dividedBy: 20),
            width: screenWidth(context,dividedBy: 2.9),


            child: widget.aadharImage == null
                ? Padding(
                  padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 15)),
                  child: Text('No image \n selected.',style: TextStyle(color: Colors.grey,fontSize: 12,fontFamily: 'Poppins'),overflow: TextOverflow.clip,),
                )
                : Image.file(widget.aadharImage,height: screenHeight(context,dividedBy: 15),width: screenWidth(context,dividedBy: 15
            ),fit: BoxFit.cover,),
          ),
        ),
      ),
    );
  }
}