import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/search_icons.dart';

class CustomerField extends StatefulWidget {
  String hintText;
  TextEditingController textEditingController;
  TextInputType keyboardType;
  CustomerField({this.hintText,this.textEditingController,this.keyboardType});
  @override
  _CustomerFieldState createState() => _CustomerFieldState();
}
class _CustomerFieldState extends State<CustomerField> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: 40,),left: screenWidth(context,dividedBy: 15)),
        child: Container(
          height: screenHeight(context,dividedBy: 13),
          width: screenWidth(context,dividedBy: 1.2),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black54),
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15),),

          ),
          child: TextField(controller:widget.textEditingController,style: TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 18),
            keyboardType:widget.keyboardType,
            autofocus: false,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top:screenHeight(context,dividedBy: 700,),left: screenWidth(context,dividedBy: 15),),
                labelText: widget.hintText,
                labelStyle: TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 13),
                border: InputBorder.none
            ),
          ),
        ),
      ),
    );
  }
}
