
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/models/areal_list_model.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:jewel_app/ui/widgets/custom_find_dropdown.dart';

class AreaCard extends StatefulWidget {
  final String label;
  final TextEditingController textEditingController;
  final Function onChange;
  bool autoFocus;
  final ValueChanged<int> getArea;
  String hint;
  AreaCard({
    this.label,
    this.textEditingController,
    this.onChange,
    this.autoFocus,
    this.getArea,
    this.hint
  });

  @override
  _AreaCardState createState() => _AreaCardState();
}

class _AreaCardState extends State<AreaCard> {
  bool isLoading = false;
  String statusCode;
  String message;
  List<String> areaList = [];
  List<int> areaId = [];
  String hint = "Area";
  AreaListResponse responseModel = new AreaListResponse();
  void getAreaList() async {
    var client = http.Client();
    try {
      var res = await client.get(
          Urls.findArea,
          headers: {"Authorization": "Token " + ObjectFactory().appHive.getToken()}
      );
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(res.body);
      // statusCode = jsonResponse['status'];
      message = jsonResponse['message'];
      print("messs" + jsonResponse.toString());
      // statusNames=jsonResponse['data']["name"];
      print(jsonResponse["data"]);
      await setState(() {
        responseModel =
            AreaListResponse.fromJson(convert.jsonDecode(res.body));
      });

      for (int i = 0; i < responseModel.data.length; i++) {
        areaList.add(responseModel.data[i].name);
        areaId.add(responseModel.data[i].id);
      }
      print(areaId);
      print("sas"+areaList.toString());

    } finally {
      client.close();
    }
  }
  @override
  void initState() {
    Text(hint,style: TextStyle(fontSize: 16,color: Color(0xFFf0f0f0)));
    getAreaList();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: screenHeight(context,dividedBy: 15),
      // width: screenWidth(context,dividedBy: 1.25),
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.black54),
      //   borderRadius: BorderRadius.circular(10),
      //   // color: Constants.kitGradients[2],
      // ),
      child: CustomFindDropdown(selectedItem:widget.hint ?? "Area",
        items:areaList,titleStyle: TextStyle(fontSize: 16,color: Color(0xFFf0f0f0)),
        onChanged: (String item){
        widget.getArea(areaId[areaList.indexOf(item)]);
        print(areaId[areaList.indexOf(item)].toString());
        },
      ),
      // child: Padding(
      //   padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 100)),
      //
      //   child: TextField(style: TextStyle(fontSize:screenHeight(context,dividedBy: 50) ,
      //
      //   ),
      //     autofocus: widget.autoFocus,
      //     maxLines: null,
      //     controller: widget.textEditingController,
      //     onChanged: widget.onChange,
      //     decoration: InputDecoration(
      //         border: InputBorder.none,
      //         hintText: widget.label,
      //         hintStyle: TextStyle(fontSize: screenHeight(context,dividedBy: 50))
      //     ),
      //   ),
      // ),
    );
  }
}
