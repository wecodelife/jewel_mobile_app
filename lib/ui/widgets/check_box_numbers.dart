import 'package:checkbox_grouped/checkbox_grouped.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/build_clickable_icon.dart';
import 'package:jewel_app/ui/widgets/check_box_icons.dart';

class CheckBoxNumber extends StatefulWidget {

  ValueChanged onChanged;
  String quest;
  CheckBoxNumber({this.quest,this.onChanged});
  @override
  _CheckBoxNumberState createState() => _CheckBoxNumberState();
}

class _CheckBoxNumberState extends State<CheckBoxNumber> {
  bool one=false;
  bool two=false;
  bool three=false;
  int value;
  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 3.0,
      child: Container(
        child: Padding(
          padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 20),bottom:screenHeight(context,dividedBy: 20)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(widget.quest,style: TextStyle(color: Colors.black,fontSize:14,fontFamily: 'Poppins'),),
              ),
              Padding(
                padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 400),),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                        onTap: (){
                          setState(() {
                            one=!one;
                            two=false;
                            three=false;
                            value=1;
                            widget.onChanged(value);

                            print( "value =" + value.toString());
                          });
                        },
                        child: BuildClickableIcon(checkBoxData: "1",iconData:one==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),

                    Padding(
                      padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 30)),
                      child: GestureDetector(
                          onTap: (){
                            setState(() {
                              two=!two;
                              three=false;
                              one=false;
                              value=2;
                              widget.onChanged(value);
                              print( "value =" + value.toString());

                            });
                          },
                          child: BuildClickableIcon(checkBoxData: "2",iconData:two==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 30)),
                      child: GestureDetector(
                          onTap: (){
                            setState(() {
                              three=!three;
                              two=false;
                              one=false;
                              value=3;
                              widget.onChanged(value);
                              print( "value =" + value.toString());

                            });
                          },
                          child: BuildClickableIcon(checkBoxData: "3",iconData:three==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

      ),
    );
  }
}
