import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';

class PhoneNumberBox extends StatefulWidget {
  String phoneNumber1;
  PhoneNumberBox({this.phoneNumber1});
  @override
  _PhoneNumberBoxState createState() => _PhoneNumberBoxState();
}

class _PhoneNumberBoxState extends State<PhoneNumberBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 40,),
            left: screenWidth(context, dividedBy: 15)),
        child: Container(
          height: screenHeight(context, dividedBy: 15),
          width: screenWidth(context, dividedBy: 1.2),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black54),
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15),),

          ),
          child: Padding(
            padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 12)),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(widget.phoneNumber1, style: TextStyle(
                  color: Colors.black, fontFamily: 'Poppins', fontSize: 16),),
            ),
          ),
        ),
      ),
    );
  }
}









































































































































































