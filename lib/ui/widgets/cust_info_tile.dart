import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/screens/customer_info.dart';
import 'package:jewel_app/ui/screens/customer_update_page.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable

class CustomerInfoTile extends StatefulWidget {
  String name;
  String houseNO;
  String city;
  String phoneNum;
  int customerId;
  String bride;
  String guardian;
  String house;
  int area;
  String agent;
  String areaName;
  String place;
  String district;
  String marriageDate;
  String remarks;
  String ph1;
  String ph2;
  String ph3;
  String phId;
  String phStatus;
  String phCustomer;
  String phId2;
  String phCustomer2;
  String phStatus2;
  String phId3;
  String phCustomer3;
  String phStatus3;
  CustomerInfoTile(
      {this.agent,
      this.phStatus,
      this.phId,
      this.phCustomer,
      this.phId2,
      this.phCustomer2,
      this.phStatus2,
      this.phId3,
      this.phCustomer3,
      this.phStatus3,
      this.name,
      this.houseNO,
      this.city,
      this.phoneNum,
      this.customerId,
      this.areaName,
      this.bride,
      this.remarks,
      this.marriageDate,
      this.district,
      this.place,
      this.area,
      this.guardian,
      this.house,
      this.ph1,
      this.ph2,
      this.ph3});
  @override
  _CustomerInfoTileState createState() => _CustomerInfoTileState();
}

class _CustomerInfoTileState extends State<CustomerInfoTile> {
  _calling(String phoneNumber) async {
    String url = 'tel:+' + phoneNumber;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: screenHeight(context, dividedBy: 50),
          left: screenWidth(context, dividedBy: 30),
          right: screenWidth(context, dividedBy: 30)),
      child: Container(
        child: Card(
          elevation: 3,
          child: ListTile(
              isThreeLine: true,
              contentPadding:
                  EdgeInsets.only(left: screenHeight(context, dividedBy: 30)),
              title: Text(
                widget.name,
                style: TextStyle(
                    fontSize: 16,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Poppins'),
              ),
              subtitle: Container(
                  child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(),
                        child: Container(
                            width: screenWidth(context, dividedBy: 4),
                            child: Text(widget.houseNO + "\n" + widget.city,
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black54,
                                    fontFamily: 'Segoe',
                                    fontWeight: FontWeight.w800))),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 100),
                        bottom: screenHeight(context, dividedBy: 40),
                        right: screenWidth(context, dividedBy: 200)),
                    child: Container(
                        height: screenHeight(context, dividedBy: 26),
                        width: screenWidth(context, dividedBy: 7),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Constants.kitGradients[3],
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(-0.2, -0.2),
                                color: Color(0xFFDBE7EB),
                                blurRadius: 2,
                                spreadRadius: 2,
                              ),
                              BoxShadow(
                                offset: Offset(0.2, 0.2),
                                color: Color(0xFFDBE7EB),
                                blurRadius: 2,
                                spreadRadius: 2,
                              )
                            ]),
                        child: widget.phoneNum != null
                            ? Padding(
                                padding: EdgeInsets.only(
                                    top: screenHeight(context, dividedBy: 200),
                                    bottom:
                                        screenHeight(context, dividedBy: 200)),
                                child: GestureDetector(
                                  onTap: () {
                                    _calling(widget.phoneNum);
                                  },
                                  child: SvgPicture.asset(
                                    "assets/icons/Call.svg",
                                    height:
                                        screenHeight(context, dividedBy: 70),
                                    color: Constants.kitGradients[1],
                                  ),
                                ))
                            : Container()),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: screenWidth(context, dividedBy: 30),
                      bottom: screenHeight(context, dividedBy: 40),
                    ),
                    child: GestureDetector(
                      child: Container(
                          height: screenHeight(context, dividedBy: 26),
                          width: screenWidth(context, dividedBy: 2.8),
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[3],
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(-0.2, -0.2),
                                  color: Color(0xFFDBE7EB),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                ),
                                BoxShadow(
                                  offset: Offset(0.2, 0.2),
                                  color: Color(0xFFDBE7EB),
                                  blurRadius: 2,
                                  spreadRadius: 2,
                                )
                              ]),
                          child: Center(
                              child: Text(
                            "Add Feild Report",
                            style: TextStyle(
                                fontSize: 12,
                                color: Constants.kitGradients[1],
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600),
                            overflow: TextOverflow.clip,
                          ))),
                      onTap: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CustomerUpdate(
                                      bride: widget.bride,
                                      guardian: widget.guardian,
                                      area: widget.area,
                                      district: widget.district,
                                      house: widget.house,
                                      marriageDate: widget.marriageDate,
                                      place: widget.place,
                                      ph1: widget.ph1,
                                      ph2: widget.ph2,
                                      ph3: widget.ph3,
                                      areaName: widget.areaName,
                                      id: widget.customerId.toString(),
                                      agent: widget.agent,
                                      phCustomer: widget.phCustomer,
                                      phId: widget.phId,
                                      phStatus: widget.phStatus,
                                      phCustomer2: widget.phCustomer2,
                                      phId2: widget.phId2,
                                      phStatus2: widget.phStatus2,
                                      phCustomer3: widget.phCustomer3,
                                      phId3: widget.phId3,
                                      phStatus3: widget.phStatus3,
                                  name: widget.name,
                                  city: widget.city,
                                  houseNumber: widget.houseNO,
                                  custId: widget.customerId,
                                  phoneNumber: widget.phoneNum,
                                    )),
                            (route) => true);
                      },
                    ),
                  ),
                ],
              ))),
        ),
      ),
    );
  }
}
