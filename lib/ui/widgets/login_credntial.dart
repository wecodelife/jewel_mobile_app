import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';
class LoginCredentials extends StatefulWidget {
  @override
  TextEditingController customerName;
  String hintText;
  LoginCredentials({this.customerName,this.hintText,});
  _LoginCredentialsState createState() => _LoginCredentialsState();
}

class _LoginCredentialsState extends State<LoginCredentials> {
  bool obscureText;
  bool obscureTextUser=false;
  @override
  void initState() {
    obscureText=true;

    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
               padding:  EdgeInsets.only(left: screenWidth(context,dividedBy:40,),right:screenWidth(context,dividedBy:40 ) ),
                          child: Card(
                            elevation: 3.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)
                            ),
                            child: Container(
                              height: screenHeight(context,dividedBy: 14),
                              width: screenWidth(context,dividedBy: 1.1),
                              decoration: BoxDecoration(
                                color: Constants.kitGradients[1],
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: TextField(
                               // onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => DestinationPage()));},
                                style: TextStyle(color:Constants.kitGradients[4],fontFamily: 'Josefins Sans',fontWeight: FontWeight.w400,fontSize: 20),
                                cursorColor: Constants.kitGradients[0],
                                controller: widget.customerName,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: screenWidth(context,dividedBy:15),top: screenHeight(context,dividedBy:50)),

                                  hintText: widget.hintText,
                                  suffixIcon:widget.hintText=="Password"? GestureDetector(
                                    onTap: (){
                                      setState(() {
                                        obscureText=!obscureText;
                                      });

                                    },


                                      child: Padding(
                                        padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 80),right: screenWidth(context,dividedBy: 20)),
                                        child: obscureText==true ? Icon(Icons.visibility,color: Colors.grey,):Icon(Icons.visibility_off,color: Colors.grey,),
                                                )):Container(height: 1,width: 1,),
                                  hintStyle: TextStyle(color:Constants.kitGradients[4],fontFamily: 'Josefin Sans',fontWeight: FontWeight.w400,fontSize: 20),
                                  border: InputBorder.none,
                                ),
                                obscureText: widget.hintText=="Password"?obscureText:obscureTextUser,
                              ),
                            ),
                          ),





      ),
            );
  }
}
