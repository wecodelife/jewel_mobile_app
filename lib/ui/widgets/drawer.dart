import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/screens/login_page.dart';
import 'package:jewel_app/ui/widgets/drawer_page_icons.dart';


class DrawerPage extends StatefulWidget {
  String userName;
  DrawerPage();
  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  UserBloc userBloc =UserBloc();
  String LogOutToken;
  bool isLoading =false;
  @override
  void initState() {

    setState(() {
       isLoading==false;
    });
    userBloc.customerLogoutResponse.listen((event) {

      if(event.status==200){

        ObjectFactory().appHive.deleteToken();
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context )=> Login()), (route) => false);

      }

      else {

        showToast(event.message);
      }



    });


    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2),
      child: new Drawer(
          child: Column(
           children: [
             Padding(
            padding: EdgeInsets.only(
              top: screenHeight(context, dividedBy: 10),
              left: screenWidth(context, dividedBy: 40),
            ),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      right: screenWidth(context, dividedBy: 80)),
                  child: CircleAvatar(
                    radius: 25,
                    backgroundColor: Constants.kitGradients[6],
                    child: Icon(Icons.person,color: Colors.white,size: 50,),
                  ),
                ),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 150),
                            left: screenWidth(context, dividedBy: 80),
                            right: screenWidth(context, dividedBy: 40)),
                        child: Text(
                          ObjectFactory().appHive.getCustomerName().toString(),
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                            fontFamily: 'Segoe UI',
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ]),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 15)),
            child: Divider(
              color: Colors.white70,
              thickness: 2.0,
            ),
          ),
            Padding(
            padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 40)),
              child: GestureDetector(
                onTap:(){
                  setState(() {
                    isLoading=true;
                  });

                  userBloc.customerLogout();


                },
                child: isLoading==false ? DrawerPageIcons(
                options: "Logout",
                iconImage: Icons.logout,
            ):CircularProgressIndicator(
                  valueColor:new AlwaysStoppedAnimation<Color>(Constants.kitGradients[3]) ,
                )
              ),
          ),


        ],
      )
      ),
    );
  }
}
