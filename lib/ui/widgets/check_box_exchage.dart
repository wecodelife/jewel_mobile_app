import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/build_clickable_icon.dart';
import 'package:jewel_app/ui/widgets/check_box_icons.dart';
import 'package:jewel_app/ui/widgets/form_feild.dart';

class CheckBoxExchange extends StatefulWidget {
  TextEditingController textEditingController=new TextEditingController();
  ValueChanged onChanged;
  String quest;
  CheckBoxExchange({this.onChanged,this.quest,this.textEditingController});
  @override
  _CheckBoxExchangeState createState() => _CheckBoxExchangeState();
}

class _CheckBoxExchangeState extends State<CheckBoxExchange> {
  bool yes=false;
  bool no=false;
  bool answer;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Container(
        child: Padding(
          padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 30),bottom: screenHeight(context,dividedBy: 50)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,


            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(widget.quest,style: TextStyle(color: Colors.black,fontSize:14,fontFamily: 'Poppins'),),
              ),
              Padding(
                padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 700),left: screenWidth(context,dividedBy: 80)),
                child: Row(
                  children: [
                    GestureDetector(
                        onTap: (){
                          setState(() {
                            yes=!yes;
                            no=false;
                            answer=true;
                            widget.onChanged(answer);

                          });
                        },
                        child: BuildClickableIcon(checkBoxData: "Yes",iconData:yes==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),

                    Padding(
                      padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 30)),
                      child: GestureDetector(
                          onTap: (){
                            setState(() {
                              no=!no;
                              yes=false;
                              answer=false;
                              widget.onChanged(answer);
                            });
                          },
                          child: BuildClickableIcon(checkBoxData: "No",iconData:no==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 700),left: screenWidth(context,dividedBy: 40)),
                      child: Text("(if Yes):",style: TextStyle(color: Colors.black,fontSize:14,fontFamily: 'Poppins',),),
                    ),

                   yes==true ? Padding(
                      padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 40),top: screenHeight(context,dividedBy:700)),
                      child: Container(
                        height: screenHeight(context,dividedBy: 15),
                        width: screenWidth(context,dividedBy: 4),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black54),
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(15),),

                        ),
                        child: TextField(
                          controller:widget.textEditingController ,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(top:screenHeight(context,dividedBy: 700,),left: screenWidth(context,dividedBy: 25),),
                              hintText: "Gram's",
                              hintStyle: TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 16
                              ),
                              border: InputBorder.none

                          ),


                        ),
                      ),
                    ):Container()
                  ],
                ),
              ),
            ],
          ),
        ),

      ),
    );
  }
}
