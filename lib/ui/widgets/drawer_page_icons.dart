import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';

class DrawerPageIcons extends StatefulWidget {
  String options;
  IconData iconImage;
  DrawerPageIcons({this.options,this.iconImage});
  @override
  _DrawerPageIconsState createState() => _DrawerPageIconsState();
}

class _DrawerPageIconsState extends State<DrawerPageIcons> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,dividedBy: 2.8),
      child: Row(

        children: [

          Padding(
            padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 30)),
            child:Icon(widget.iconImage,color: Constants.kitGradients[7],)
          ),

          Container(
              width: screenWidth(context,dividedBy: 5),

              child: Text(widget.options,style: TextStyle(color: Constants.kitGradients[7],fontFamily: 'Segeo UI',fontSize: 22),overflow: TextOverflow.clip,))
        ],
      ),
    );
  }
}
