import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';

class Button extends StatefulWidget {
  bool loading;
  String ButtonName;
  Button({this.loading,this.ButtonName});
  @override
  _ButtonState createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: screenHeight(context,dividedBy: 17),
        width: screenWidth(context,dividedBy: 2.3),

        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Constants.kitGradients[3],
        ),
        child: Align(
            alignment: Alignment.center,
            child: widget.loading==false? Text(
              widget.ButtonName,
              style: TextStyle(
                  fontSize: 18,
                  color: Constants.kitGradients[1]),
            ):CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            )),
      ),
    );
  }
}
