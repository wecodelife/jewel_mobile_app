import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';

class ImageUploads extends StatefulWidget {
  ValueChanged onChanged;
  ImageUploads({this.onChanged});
  @override
  _ImageUploadsState createState() => _ImageUploadsState();
}

class _ImageUploadsState extends State<ImageUploads> {

  final picker = ImagePicker();
  File imagePicked;

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery,imageQuality: 25 );
    if (pickedFile != null){
      setState(() {
        imagePicked=File(pickedFile.path);
      });
      widget.onChanged(imagePicked);
    }
  }
  Future getImageCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera,imageQuality: 25);
    if (pickedFile != null){
      setState(() {
        imagePicked=File(pickedFile.path);
      });
      widget.onChanged(File(pickedFile.path));
    }
  }

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 40)),
      child: ClipRRect(

        borderRadius: BorderRadius.circular(10),
        child: GestureDetector(
          onTap: (){
            showBottomSheet(context: context, builder: (context)=>Container(
              color: Colors.white,
              height: screenHeight(context,dividedBy: 4),
              width: screenWidth(context,dividedBy: 1),
              child:Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                   Padding(
                     padding: EdgeInsets.only(right: screenWidth(context,dividedBy: 10)),
                     child: GestureDetector(
                         onTap: (){
                           Navigator.pop(context);
                         },
                         child: Text("Cancel",style: TextStyle(color:Constants.kitGradients[3],fontFamily: "Poppins",fontSize: 12, ),)),
                   ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     children: [
                       GestureDetector(
                         onTap: (){
                               getImageCamera();
                               Navigator.pop(context);
                                    },

                           child: Icon(Icons.camera,size: 50,color: Constants.kitGradients[3],)),

                       GestureDetector(
                           onTap:() {
                             getImageGallery();
                             Navigator.pop(context);
                                  },

                           child: Icon(Icons.image,size: 50,color:Constants.kitGradients[3],)),
                     ],
                  ),
                ],
              )

            ));
          },
          child: Container(
            height: screenHeight(context,dividedBy: 7),
            width: screenWidth(context,dividedBy: 3),


            child: imagePicked== null
                ? Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('No image ',style: TextStyle(color: Colors.grey,fontSize: 12,fontFamily: 'Poppins'),overflow: TextOverflow.clip,),
                    Icon(Icons.upload_rounded,size: 20,color: Colors.grey,)
                  ],
                )
                : Image.file(imagePicked,height: screenHeight(context,dividedBy: 15),width: screenWidth(context,dividedBy: 15
            ),fit: BoxFit.cover,),
          ),
        ),
      ),
    );
  }
}