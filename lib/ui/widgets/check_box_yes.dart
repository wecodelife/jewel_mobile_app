import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/build_clickable_icon.dart';
import 'package:jewel_app/ui/widgets/check_box_icons.dart';

class CheckBoxYes extends StatefulWidget {
  ValueChanged onChanged;
  String quest;
  CheckBoxYes({this.onChanged,this.quest,});
  @override
  _CheckBoxYesState createState() => _CheckBoxYesState();
}

class _CheckBoxYesState extends State<CheckBoxYes> {
  bool yes=false;
  bool no=false;
  bool decision=false;
  bool yesClicked=true;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 50),right: screenWidth(context,dividedBy: 14),top: screenHeight(context,dividedBy: 60)),
      child: Card(
        elevation: 3,
        child: Container(
          child: Padding(
            padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 20),bottom:screenWidth(context,dividedBy: 20),top: screenWidth(context,dividedBy:60 ) ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,

              children: [
                Padding(
                  padding:  EdgeInsets.only(),
                  child: Text(widget.quest,style: TextStyle(color: Colors.black,fontSize:14,fontFamily: 'Poppins'),),
                ),
                Padding(
                  padding: EdgeInsets.only(top:screenHeight(context,dividedBy: 100),left: screenWidth(context,dividedBy: 10)),
                  child: Row(
                    children: [
                      GestureDetector(
                          onTap: (){
                            setState(() {
                              yes=!yes;
                              no=false;
                              decision=true;
                              widget.onChanged(decision);


                            });
                          },
                          child: BuildClickableIcon(checkBoxData: "Yes",iconData:yes==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),

                      Padding(
                        padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 4)),
                        child: GestureDetector(
                            onTap: (){
                              setState(() {
                                no=!no;
                                yes=false;
                                decision=false;
                                widget.onChanged(decision);
                              });
                            },
                            child: BuildClickableIcon(checkBoxData: "No",iconData:no==true  ? CheckBox.radio_button_checked:CheckBox.radio_button_unchecked,)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

        ),
      ),
    );
  }
}
