import 'package:checkbox_grouped/checkbox_grouped.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

class DateFormFeild extends StatefulWidget {

  ValueChanged date;
  String hint;
   DateFormFeild({this.date,this.hint});
  @override
  _DateFormFeildState createState() => _DateFormFeildState();
}

class _DateFormFeildState extends State<DateFormFeild> {
  bool dateSelected=false;
  DateTime selectedDate = DateTime.now();
  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dateSelected=true;
        widget.date(selectedDate);
      });
  }
  @override
  void initState() {


    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(

      child: GestureDetector(

        child: Container(
            height: screenHeight(context,dividedBy: 15),
            width: screenWidth(context,dividedBy: 1.2),
            decoration: BoxDecoration(
            border: Border.all(color: Colors.black54),
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(13),),),

            child:
            Row(
              children: [
                dateSelected==true ?
                Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 20)),
                  child:
                  Text(
                    "${selectedDate.toLocal()}".split(' ')[0],
                    style: TextStyle(fontSize: 20,),
                  ),
                ):Padding(
                  padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 15)),
                  child: Text(widget.hint!="null"?widget.hint:"Pick Date",style:TextStyle( color:Colors.black,fontFamily: 'Poppins', fontSize: 16)),
                 ),
              ],
            ),


        ),
        onTap: (){
          _selectDate(context);

          //widget.date(selectedDate);

        },
      ),
    );
  }
}
