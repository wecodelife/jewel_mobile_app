import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/check_box_icons.dart';

class BuildClickableIcon extends StatefulWidget {
  final IconData iconData;
  final double size;
  String checkBoxData;

  BuildClickableIcon({this.iconData, this.size,this.checkBoxData});

  @override
  _BuildClickableIconState createState() => _BuildClickableIconState();
}

class _BuildClickableIconState extends State<BuildClickableIcon> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Row(
        children: [
          Icon(widget.iconData,color: Constants.kitGradients[3],),
          Padding(
            padding: EdgeInsets.only(left: screenWidth(context,dividedBy:100),right: screenWidth(context,dividedBy:100)),
            child: Text(widget.checkBoxData,style: TextStyle(color: Colors.black87,fontSize: 16,fontFamily: 'Poppins',fontWeight: FontWeight.w400),),
          ),
          
        ],
      ),
      
      
      
             


      

    );
  }
}
