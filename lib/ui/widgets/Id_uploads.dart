import 'package:flutter/material.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';

class IdUploads extends StatefulWidget {
  String title;
  IdUploads({this.title});
  @override
  _IdUploadsState createState() => _IdUploadsState();
}

class _IdUploadsState extends State<IdUploads> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: screenHeight(context,dividedBy: 20),
        width: screenWidth(context,dividedBy: 2.7),
        decoration: BoxDecoration(color: Constants.kitGradients[3],
        borderRadius: BorderRadius.circular(10)),

        child: Align(
            alignment: Alignment.center,
            child: Text(widget.title,
              style: TextStyle(
                  fontSize: 14,
                  color: Constants.kitGradients[1]),
            )),
      ),
    );
  }
}
