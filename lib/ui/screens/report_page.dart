import 'dart:io';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/models/feild_report_request.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/screens/customer_info.dart';
import 'package:jewel_app/ui/widgets/Id_uploads.dart';
import 'package:jewel_app/ui/widgets/Image%20_uploads.dart';
import 'package:jewel_app/ui/widgets/adhar_image.dart';
import 'package:jewel_app/ui/widgets/button.dart';
import 'package:jewel_app/ui/widgets/check_box_exchage.dart';
import 'package:jewel_app/ui/widgets/check_box_numbers.dart';
import 'package:jewel_app/ui/widgets/check_box_yes.dart';
import 'package:jewel_app/ui/widgets/cheqque_image.dart';
import 'package:jewel_app/ui/widgets/date_form_feild.dart';
import 'package:jewel_app/ui/widgets/drawer.dart';
import 'package:jewel_app/ui/widgets/drop_down_feildreport.dart';
import 'package:jewel_app/ui/widgets/form_feild.dart';
import 'package:jewel_app/ui/widgets/phone_number_box.dart';
import 'package:jewel_app/ui/widgets/phone_number_button.dart';

class ReportPage extends StatefulWidget {
  String name;
  String houseNumber;
  String city;
  int custId;
  String phoneNumber;
  String dateMarriage;
  ReportPage({this.phoneNumber,this.name, this.houseNumber, this.city, this.custId,this.dateMarriage});

  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  UserBloc userBloc = UserBloc();
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  int financial;
  String marriageDate;
  String marriageSet;
  bool gold = false;
  bool oldYes = false;
  bool oldNo = false;
  bool emiYes = false;
  int loanOne;
  int noEmiOne;
  bool goldAdvance = false;
  bool altJewel = false;
  bool altAdvance = false;
  bool newMarriage = false;
  File taxImg;
  File cheqImg;
  File aadharImg;
  bool isLoading = false;
  Position agentLocation;
  String lat;
  String long;
  Geolocator geolocator = Geolocator();
  int phoneNumberCount;
  bool onClicked =false;
  bool onButton2 =false;


  TextEditingController goldTextEditingController = new TextEditingController();
  TextEditingController exchangeGoldTextEditingController =new TextEditingController();
  TextEditingController tokenAdavnceTextEditingController =
      new TextEditingController();
  TextEditingController rateAdvanceTextEditingController =
      new TextEditingController();
  TextEditingController altAdvanceTextEditingController =
      new TextEditingController();
  TextEditingController newMarriagePartyTextEditingController =
      new TextEditingController();
  TextEditingController phoneNumber1TextEditingController =
         new TextEditingController();
  TextEditingController phoneNumber2TextEditingController =
      new TextEditingController();
  TextEditingController phoneNumber3TextEditingController =
  new TextEditingController();

  @override
  void initState() {
    userBloc.addFieldReportResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.status == 200 || event.status == 201) {
        //navigate
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => CustomerInfo(
            )),
            (route) => false);

           showToast(event.message);
      } else {
        showToast(event.message);
      }
    });

    _getLocation().then((currentLocation) {

      setState(() {
        agentLocation = currentLocation;
        lat=agentLocation.latitude.toString();
        long=agentLocation.longitude.toString();

      });
      print(agentLocation.toString() + "agent location");

    });
    super.initState();
  }

  Future<Position> _getLocation() async {
    var currentLocation;
    try {
      currentLocation = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
      print(currentLocation);
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawer: DrawerPage(),
      body: ListView(

          children: [
        Column(
            crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(
              top: screenHeight(
                context,
                dividedBy: 20,
              ),
              left: screenWidth(context, dividedBy: 30),
            ),
            child: GestureDetector(
              onTap: () {
                _globalKey.currentState.openDrawer();
              },
              child: Container(
                height: screenHeight(
                  context,
                  dividedBy: 17,
                ),
                width: screenWidth(context, dividedBy: 11),
                child: Icon(Icons.sort, color: Colors.black, size: 40),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(context, dividedBy: 30),
                left: screenWidth(context, dividedBy: 8)),
            child: Text(
              widget.name,
              style: TextStyle(
                  fontSize: 22,
                  color: Constants.kitGradients[4],
                  fontWeight: FontWeight.w800,
                  fontFamily: 'Poppins'),
            ),
          ),
          Row(children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(left: screenWidth(context, dividedBy: 8)),
                  child: Container(
                      child: Text(widget.houseNumber + "\n" + widget.city,
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.black54,
                              fontFamily: 'Segoe',
                              fontWeight: FontWeight.w800))),
                ),
              ],
            ),
          ]),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20),
                vertical: screenHeight(context, dividedBy: 200)),
            child: Divider(
              thickness: 7.0,
              color: Constants.kitGradients[3],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 4)),
            child: Text(
              "Add Feild Report",
              style: TextStyle(
                  fontSize: 22,
                  color: Constants.kitGradients[4],
                  fontWeight: FontWeight.w800,
                  fontFamily: 'Poppins'),
            ),
          ),


          Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 15),
                top: screenHeight(context, dividedBy: 40)),
            child: DateFormFeild(
              hint: widget.dateMarriage.split(" ").first,
              date: (value) {
                marriageDate = value.toString().split(' ').first;
                print('date is ' + value.toString());
                print("custId" + widget.custId.toString());
              },
            ),
          ),

          widget.phoneNumber!="" ? PhoneNumberBox(phoneNumber1: widget.phoneNumber,):FormFeildBox(hintText: "Phone Number",
          textEditingController: phoneNumber1TextEditingController,),

          onClicked== false ? Padding(
            padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 2)),
            child: GestureDetector(
                onTap: (){
                  setState(() {
                    onClicked=true;
                    print("Clicked" + onClicked.toString());
                  });
                },
                child: PhoneNumberButton()),
          ):Container(),


          onClicked==true ?FormFeildBox(hintText: "Phone Number",textEditingController: phoneNumber2TextEditingController,):Container(),

          onClicked == true && onButton2==false  ? Padding(
            padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 2)),
            child: GestureDetector(
                onTap: (){
                  setState(() {
                    onButton2=true;
                    print("Clicked" + onClicked.toString());
                  });
                },
                child: PhoneNumberButton()),
          ):Container(),

          onButton2==true ?FormFeildBox(hintText: "Phone Number",textEditingController: phoneNumber3TextEditingController,):Container(),








          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(
                  context,
                  dividedBy: 60,
                ),
                left: screenWidth(context, dividedBy: 20),
                right: screenWidth(context, dividedBy: 14)),
            child: CheckBoxNumber(
              onChanged: (value) {
                financial = value;
                print("financial" + financial.toString());
              },
              quest: "Financial background",
            ),
          ),
          FormFeildBox(
            hintText: "Amount of Gold for Marriage",
            textEditingController: goldTextEditingController,
          ),
          Padding(
            padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 20),top: screenHeight(context,dividedBy:60 )),
            child: Container(
              width: screenWidth(context,dividedBy: 1.14),
              child: Card(
                elevation: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 40),
                          left: screenWidth(context, dividedBy: 15)),
                      child: Text(
                        "The affordable marriage set",
                        style: TextStyle(
                            color: Colors.black, fontSize: 14, fontFamily: 'Poppins'),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 12),
                          top: screenHeight(context, dividedBy: 60),
                           bottom:screenHeight(context,dividedBy:60) ),
                      child: DropReportPage(
                        onChanged: (value) {
                          marriageSet = value;
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 40),
                ),
            child: CheckBoxYes(
              onChanged: (decision) {
                gold = decision;
              },
              quest: "Like to exchange gold after marriage",
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(context, dividedBy: 70),
                left: screenWidth(context, dividedBy: 20),
                right: screenWidth(context, dividedBy: 14),
                ),
            child: CheckBoxExchange(
              onChanged: (answer) {
                oldYes = answer;
              },
              textEditingController: exchangeGoldTextEditingController,
              quest: "Like to exchange old jewellery",
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 40)),
            child: CheckBoxYes(
              onChanged: (decision) {
                emiYes = decision;
              },
              quest: "Like to Take no cost EMI",
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 23),
                right: screenWidth(context, dividedBy: 14),
                top: screenHeight(context,dividedBy: 60)),
            child: CheckBoxNumber(
              onChanged: (value) {
                loanOne = value;
              },
              quest: "Duration of Loan in Years (If Yes)",
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 23),
                right: screenWidth(context, dividedBy: 14),
                top: screenHeight(context,dividedBy: 60)),
            child: CheckBoxNumber(
              onChanged: (value) {
                noEmiOne = value;
              },
              quest: "Number of Emi's (If Yes)",
            ),
          ),
          Padding(
            padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 25),right: screenWidth(context,dividedBy: 14),
            top: screenHeight(context,dividedBy: 80)),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 13),top: screenHeight(context,dividedBy: 60)),
                    child: Text(
                      "Documents Required for No Cost EMi",
                      style: TextStyle(
                          color: Colors.black, fontSize: 14, fontFamily: 'Poppins'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: screenHeight(context, dividedBy: 40),
                        left: screenWidth(context, dividedBy: 15)),
                    child: Row(
                      children: [
                        IdUploads(
                          title: "Wedding Invitations",
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenWidth(context, dividedBy: 30)),
                          child: IdUploads(title: "Cheques Leafs"),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: screenHeight(context, dividedBy: 40),
                        left: screenWidth(context, dividedBy: 15),
                         bottom: screenHeight(context,dividedBy:40 )),
                    child: Row(
                      children: [
                        IdUploads(
                          title: "Tax Payment Copy",
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenWidth(context, dividedBy: 30)),
                          child: IdUploads(
                            title: "ID Card Copy",
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),



          Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 40)),
            child: CheckBoxYes(
              onChanged: (decision,) {
                setState(() {
                  goldAdvance = decision;
                  print("goldAdvance" + goldAdvance.toString());
                });
                },
              quest: "Want to book gold in advance",

            ),
          ),
          goldAdvance==true  ? Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 10),
                right: screenWidth(context, dividedBy: 6)),
            child: GestureDetector(
                onTap: () {},
                child: FormFeildBox(
                  hintText: "Token Advanced (if Yes)",
                  textEditingController: tokenAdavnceTextEditingController,
                )),
          ):Container(),
          goldAdvance==true? Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 10),
                right: screenWidth(context, dividedBy: 6)),
            child: FormFeildBox(
              hintText: "Rate Advanced (if Yes)",
              textEditingController: rateAdvanceTextEditingController,
            ),
          ):Container(),
          Padding(
            padding: EdgeInsets.only(

                left: screenWidth(context, dividedBy: 40)),
            child: CheckBoxYes(
              onChanged: (decision) {
                altJewel = decision;
              },
              quest: "Any other Jeweller came",
            ),
          ),
          Padding(
            padding: EdgeInsets.only(

                left: screenWidth(context, dividedBy: 40)),
            child: CheckBoxYes(
              onChanged: (decision) {
                setState(() {
                  altAdvance = decision;

                });
              },
              quest: "How you advanced in another jewellery",
            ),
          ),
          altAdvance== true ?Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 10),
                right: screenWidth(context, dividedBy: 6)),
            child: FormFeildBox(
              hintText: "Amount (if Yes)",
              textEditingController: altAdvanceTextEditingController,
            ),
          ):Container(),
          Padding(
            padding: EdgeInsets.only(
              
                left: screenWidth(context, dividedBy: 40)),
            child: CheckBoxYes(
              onChanged: (decision) {
                setState(() {
                  newMarriage = decision;
                  print("altAdvance " + altAdvanceTextEditingController.text);

                });
              },
              quest: "Got any information about any new marriage Party",
            ),
          ),
          newMarriage == true ?Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 10),
                right: screenWidth(context, dividedBy: 6)),
            child: FormFeildBox(
              hintText: "Nos (if Yes)",
              textEditingController: newMarriagePartyTextEditingController,
            ),

          ):Container(),
          Padding(
            padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 25),right: screenWidth(context,dividedBy: 14,),
              top: screenHeight(context,dividedBy: 60)
            ),
            child: Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  
                  Padding(
                    padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 80),left: screenWidth(context,dividedBy: 10)),
                    child: Text("Upload image",style: TextStyle(fontSize: 14,fontFamily: 'Poppins',color: Colors.black,),),
                  ),
                  
                  Padding(
                    padding: EdgeInsets.only(bottom: screenHeight(context,dividedBy: 50)),
                    child: Container(
                       width: screenWidth(context,dividedBy: 1.16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ImageUploads(
                            onChanged: (taxImage) {
                              taxImg = taxImage;
                              setState(() {

                              });
                            },
                          ),
                          taxImg!=null ?ImageUploads(
                            onChanged: (taxImage) {
                              aadharImg = taxImage;
                              setState(() {

                              });
                            },
                          ):Container(),

                          taxImg!=null && aadharImg!=null ? ImageUploads(
                          onChanged: (taxImage) {
                          cheqImg = taxImage;

                                       },
                                    ):Container(),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: screenWidth(context, dividedBy: 3.4)),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  isLoading = true;
                });
                userBloc.feildReport(
                    feildReportRequest: FeildReportRequest(
                  customer: widget.custId,
                  financialBg: financial,
                  isTakingEmi: emiYes,
                  durationOfLoan: loanOne,
                  numberOfEmi: noEmiOne,
                  isAdvanceBooking: goldAdvance,
                  isAdvancedInOther: altAdvance,
                  isAnotherJewellerCame: altJewel,
                  knowAnyNewParty: newMarriage,
                  image1: taxImg,
                  image2: aadharImg,
                  image3: cheqImg,
                  amountAdvancedInOther: altAdvanceTextEditingController.text,
                  goldAmt: goldTextEditingController.text,
                  marriageDate: marriageDate,
                  noOfParty: int.tryParse(
                          newMarriagePartyTextEditingController.text) ??
                      0,
                  rateAtBooking: rateAdvanceTextEditingController.text,
                  marriageSet: marriageSet,
                  tokenAdvance: tokenAdavnceTextEditingController.text,
                  isExchanging: gold,
                  isExchangingOld: oldYes,
                        lat:lat,
                        lon: long,
                      goldAmtExchanging: exchangeGoldTextEditingController.text,
                      phoneNumber1: widget.phoneNumber=="" ? phoneNumber1TextEditingController.text:widget.phoneNumber,
                      phoneNumber2: phoneNumber2TextEditingController.text,
                      phoneNumber3: phoneNumber3TextEditingController.text,

                ));
                print(" financial status " + financial.toString());
              },
              child: Button(
                ButtonName: "Add Report",
                loading: isLoading,
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          )
        ])
      ]),
    );
  }
}
