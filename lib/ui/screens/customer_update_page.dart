import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/models/customer_update_details.dart';
import 'package:jewel_app/src/models/customer_update_details_response_model.dart';
import 'package:jewel_app/src/models/customer_update_number.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/urls.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/screens/report_page.dart';
import 'package:jewel_app/ui/widgets/button.dart';
import 'package:jewel_app/ui/widgets/check_box_date.dart';
import 'package:jewel_app/ui/widgets/cust_info_tile.dart';
import 'package:jewel_app/ui/widgets/customer_field.dart';
import 'package:jewel_app/ui/widgets/date_form_feild.dart';
import 'package:jewel_app/ui/widgets/drawer.dart';
import 'package:jewel_app/ui/widgets/form_feild.dart';
import 'package:jewel_app/ui/widgets/remark_form_field.dart';
import 'package:jewel_app/ui/widgets/area_card.dart';
import 'package:jewel_app/ui/widgets/phone_number_button.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class CustomerUpdate extends StatefulWidget {
  String name;
  String houseNumber;
  String city;
  int custId;
  String phoneNumber;
  String bride;
  String guardian;
  String house;
  String areaName;
  int area;
  String place;
  String district;
  String marriageDate;
  String remarks;
  String ph1;
  String ph2;
  String ph3;
  String id;
  String phId;
  String phCustomer;
  String phStatus;
  String phId2;
  String phCustomer2;
  String phStatus2;
  String phId3;
  String phCustomer3;
  String phStatus3;
  String agent;
  CustomerUpdate(
      {this.bride,
      this.name,
      this.houseNumber,
      this.city,
      this.custId,
      this.phoneNumber,
      this.phId,
      this.phCustomer,
      this.phStatus,
      this.phId2,
      this.phCustomer2,
      this.phStatus2,
      this.phId3,
      this.phCustomer3,
      this.phStatus3,
      this.agent,
      this.remarks,
      this.marriageDate,
      this.district,
      this.place,
      this.area,
      this.guardian,
      this.house,
      this.ph1,
      this.ph2,
      this.ph3,
      this.areaName,
      this.id});
  @override
  _CustomerUpdateState createState() => _CustomerUpdateState();
}

class _CustomerUpdateState extends State<CustomerUpdate> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  bool date;
  bool onClicked = false;
  bool onButton2 = false;
  List<String> phoneNumbers = [];
  UserBloc userBloc = UserBloc();
  String marriageDate;
  CustomerUpdateDetailsResponseModel responseModel;
  int area;
  TextEditingController brideTextEditingController =
      new TextEditingController();
  TextEditingController guardianTextEditingController =
      new TextEditingController();
  TextEditingController houseTextEditingController =
      new TextEditingController();
  TextEditingController placeTextEditingController =
      new TextEditingController();
  TextEditingController districtTextEditingController =
      new TextEditingController();
  TextEditingController remarkTextEditingController =
      new TextEditingController();
  TextEditingController phoneNumber1TextEditingController =
      new TextEditingController();
  TextEditingController phoneNumber2TextEditingController =
      new TextEditingController();
  TextEditingController phoneNumber3TextEditingController =
      new TextEditingController();

  // Future<String> phoneNumber() {
  //   if (phoneNumber1TextEditingController.text.length == 10)
  //     phoneNumbers.add(phoneNumber1TextEditingController.text);
  //   if (phoneNumber2TextEditingController.text.length == 10)
  //     phoneNumbers.add(phoneNumber2TextEditingController.text);
  //   if (phoneNumber3TextEditingController.text.length == 10)
  //     phoneNumbers.add(phoneNumber3TextEditingController.text);
  //   return null;
  // }

  void nullCheck() async {
    if (brideTextEditingController.text != '' &&
        guardianTextEditingController.text != '' &&
        houseTextEditingController.text != '' &&
        placeTextEditingController.text != '' &&
        districtTextEditingController.text != '') {
      if ((phoneNumber1TextEditingController.text.length != 9 ||
              phoneNumber1TextEditingController.text == '') &&
          (phoneNumber2TextEditingController.text.length != 9 ||
              phoneNumber2TextEditingController.text == '') &&
          (phoneNumber3TextEditingController.text.length != 9 ||
              phoneNumber3TextEditingController.text == '')) {
        // await phoneNumber();
        print("custo" + widget.phStatus.toString());
        userBloc.updateCustomer(
            customerUpdatedDetails: CustomerUpdatedDetails(
              district: districtTextEditingController.text,
              place: placeTextEditingController.text,
              nameOfGuardian: guardianTextEditingController.text,
              marriageDate: marriageDate != null
                  ? marriageDate
                  : (date == true ? widget.marriageDate : null),
              houseName: houseTextEditingController.text,
              brideName: brideTextEditingController.text,
              area: area ?? widget.area,
              agent: int.parse(widget.agent),
            ),
            id: widget.id);
        userBloc.updateNumber(
            customerUpdateNumber: CustomerUpdateNumber(
              id: int.parse(widget.phId),
              status: int.parse(widget.phStatus),
              phoneNumber: int.parse(phoneNumber1TextEditingController.text),
              customer: int.parse(widget.phCustomer),
            ),
            id: widget.phId);

        if (phoneNumber2TextEditingController.text.length == 10) {
          userBloc.updateNumber(
              customerUpdateNumber: CustomerUpdateNumber(
                id: int.parse(widget.phId2),
                status: int.parse(widget.phStatus2),
                phoneNumber: int.parse(phoneNumber2TextEditingController.text),
                customer: int.parse(widget.phCustomer2),
              ),
              id: widget.phId2);
        }
        if (phoneNumber3TextEditingController.text.length == 10) {
          userBloc.updateNumber(
              customerUpdateNumber: CustomerUpdateNumber(
                id: int.parse(widget.phId3),
                status: int.parse(widget.phStatus3),
                phoneNumber: phoneNumber3TextEditingController.text.length == 10
                    ? int.parse(phoneNumber3TextEditingController.text)
                    : null,
                customer: int.parse(widget.phCustomer3),
              ),
              id: widget.phId3);
        }
      } else {
        showToast("Enter a valid phonenumber1");
      }
    } else {
      showToast("Fill in all Fields!");
    }
  }

  @override
  void initState() {
    print("value" + widget.marriageDate);
    if (widget.marriageDate != "null") {
      date = true;
    } else {
      date = false;
    }
    if (widget.ph2 != '') {
      onClicked = true;
    } else {
      onClicked = false;
    }
    if (widget.ph3 != '') {
      onButton2 = true;
    } else {
      onButton2 = false;
    }
    userBloc.updateCustomerResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.status == 200 || event.status == 201) {
        //navigate
        //
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => ReportPage(
                      custId: widget.custId,
                      houseNumber: widget.houseNumber,
                      city: widget.city,
                      name: widget.name,
                      phoneNumber: widget.phoneNumber,
                      dateMarriage: widget.marriageDate,
                    )),
            (route) => false);
        showToast(event.message);
      } else {
        showToast(event.message);
      }
    });

    brideTextEditingController.text = widget.bride;
    guardianTextEditingController.text = widget.guardian;
    houseTextEditingController.text = widget.house;
    placeTextEditingController.text = widget.place;
    districtTextEditingController.text = widget.district;
    phoneNumber1TextEditingController.text = widget.ph1;
    phoneNumber2TextEditingController.text = widget.ph2;
    phoneNumber3TextEditingController.text = widget.ph3;
    // area = widget.area;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawer: DrawerPage(),
      body: ListView(children: [
        SafeArea(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Padding(
              padding: EdgeInsets.only(
                top: screenHeight(
                  context,
                  dividedBy: 20,
                ),
                left: screenWidth(context, dividedBy: 30),
              ),
              child: GestureDetector(
                onTap: () {
                  _globalKey.currentState.openDrawer();
                },
                child: Container(
                  height: screenHeight(
                    context,
                    dividedBy: 17,
                  ),
                  width: screenWidth(context, dividedBy: 11),
                  child: Icon(Icons.sort, color: Colors.black, size: 40),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 30),
                  left: screenWidth(context, dividedBy: 8)),
              child: Text(
                "Customer Details",
                style: TextStyle(
                    fontSize: 22,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w800,
                    fontFamily: 'Poppins'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20),
                  vertical: screenHeight(context, dividedBy: 200)),
              child: Divider(
                thickness: 7.0,
                color: Constants.kitGradients[3],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  // left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: CustomerField(
                hintText: "Name of Bride",
                textEditingController: brideTextEditingController,
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  // left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: CustomerField(
                hintText: "Name of Guardian",
                textEditingController: guardianTextEditingController,
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  // left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: CustomerField(
                hintText: "House Name",
                textEditingController: houseTextEditingController,
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: AreaCard(
                hint: widget.areaName,
                getArea: (value) {
                  setState(() {
                    area = value;
                  });
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  // left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: CustomerField(
                hintText: "Place",
                textEditingController: placeTextEditingController,
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  // left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: CustomerField(
                hintText: "District",
                textEditingController: districtTextEditingController,
                keyboardType: TextInputType.text,
              ),
            ),
            Padding(
                padding: EdgeInsets.only(
                    left: screenWidth(context, dividedBy: 15),
                    top: screenHeight(context, dividedBy: 25)),
                child: CheckBoxDate(
                  decision: widget.marriageDate != "null" ? true : false,
                  onChanged: (value) {
                    setState(() {
                      date = value;
                    });
                  },
                )),
            date == true
                ? Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 15),
                        top: screenHeight(context, dividedBy: 25)),
                    child: DateFormFeild(
                      hint: widget.marriageDate.split(" ").first,
                      date: (value) {
                        marriageDate = value.toString().split(' ').first;
                      },
                    ),
                  )
                : Container(),
            // Padding(
            //   padding: EdgeInsets.only(
            //       // left: screenWidth(context, dividedBy: 15),
            //       top: screenHeight(context, dividedBy: 40)),
            //   child:RemarkFormFieldBox(
            //     hintText: "Remarks",
            //     textEditingController: remarkTextEditingController,
            //     keyboardType: TextInputType.text,
            //   ),
            // ),
            Padding(
              padding: EdgeInsets.only(
                  // left: screenWidth(context, dividedBy: 15),
                  top: screenHeight(context, dividedBy: 40)),
              child: CustomerField(
                hintText: "Phone Number",
                textEditingController: phoneNumber1TextEditingController,
                keyboardType: TextInputType.number,
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(
            //       // left: screenWidth(context, dividedBy: 15),
            //       top: screenHeight(context, dividedBy: 40)),
            //   child:CustomerField(
            //     hintText: "Phone Number",
            //     textEditingController: phoneNumber2TextEditingController,
            //     keyboardType: TextInputType.number,
            //   ),
            // ),
            // Padding(
            //   padding: EdgeInsets.only(
            //     // left: screenWidth(context, dividedBy: 15),
            //       top: screenHeight(context, dividedBy: 40)),
            //   child:CustomerField(
            //     hintText: "Phone Number",
            //     textEditingController: phoneNumber3TextEditingController,
            //     keyboardType: TextInputType.number,
            //   ),
            // ),
            onClicked == false
                ? Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 2)),
                    child: GestureDetector(
                        onTap: () {
                          setState(() {
                            onClicked = true;
                            print("Clicked" + onClicked.toString());
                          });
                        },
                        child: PhoneNumberButton()),
                  )
                : Container(),

            onClicked == true
                ? CustomerField(
                    hintText: "Phone Number",
                    textEditingController: phoneNumber2TextEditingController,
                  )
                : Container(),

            onClicked == true && onButton2 == false
                ? Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 2)),
                    child: GestureDetector(
                        onTap: () {
                          setState(() {
                            onButton2 = true;
                            print("Clicked" + onClicked.toString());
                          });
                        },
                        child: PhoneNumberButton()),
                  )
                : Container(),

            onButton2 == true
                ? CustomerField(
                    hintText: "Phone Number",
                    textEditingController: phoneNumber3TextEditingController,
                  )
                : Container(),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 3.4),
                  vertical: screenHeight(context, dividedBy: 25)),
              child: GestureDetector(
                onTap: () async {
                  print("value" + phoneNumbers.toString());
                  setState(() {
                    isLoading = true;
                  });
                  nullCheck();
                },
                child: Button(
                  ButtonName: "Update Details",
                  loading: isLoading,
                ),
              ),
            ),
          ]),
        )
      ]),
    );
  }

  @override
  void dispose() {
    phoneNumbers.clear();
    super.dispose();
  }
}
