import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/models/login_request.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/screens/customer_info.dart';
import 'package:jewel_app/ui/screens/customer_update_page.dart';
import 'package:jewel_app/ui/widgets/login_credntial.dart';
import 'package:http/http.dart' as http;

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}
class _LoginState extends State<Login> {
   String customerName;
   String tok="";
   bool isLoading = false;
  TextEditingController textEditingControllerUserId =
  new TextEditingController();
  TextEditingController textEditingControllerPassword =
  new TextEditingController();
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    // TODO: implement initState

    userBloc.loginResponse.listen((event) async{

      setState(() {
        isLoading=false;
      });
      if(event.status==200) {
        ObjectFactory().appHive.hivePut(value: event.data.token.toString(),key: "token");
        await ObjectFactory().appHive.putToken(token: event.data.token.toString());
        await ObjectFactory().appHive.customerNamePut(token: event.data.user.name);
        print("Token :" + event.data.token.toString());

        Navigator.push(context,
            MaterialPageRoute(builder: (context) => CustomerInfo()));

      }
      else if(event.status==201)
        {
          print("Token :" + event.data.token.toString());
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => Login()));
      }
      else{
        showToast(event.message);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: screenHeight(context,dividedBy: 1),
            width: screenWidth(context,dividedBy: 1),
            child: Stack(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 8),
                          left: screenWidth(context, dividedBy: 40),
                          right: screenWidth(context, dividedBy: 40)),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Container(
                            height: screenHeight(context, dividedBy: 1.6),
                            width: screenWidth(context, dividedBy: 1.03),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                // Padding(
                                //   padding:  EdgeInsets.only(top:screenHeight(context,dividedBy: )),
                                //   child: SvgPicture.asset("assets/icons/JewLogo.svg"),
                                // ),

                                Padding(
                                  padding: EdgeInsets.only(
                                      top: screenHeight(context, dividedBy: 18)),
                                  child: Image.asset("assets/icons/logo.jpeg",height: screenHeight(context,dividedBy:7),width: screenWidth(context,dividedBy:1.2 ),)
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: screenHeight(context, dividedBy: 8)),
                                  child: LoginCredentials(
                                    hintText: "Username",
                                    customerName: textEditingControllerUserId,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: screenHeight(context, dividedBy: 60)),
                                  child: LoginCredentials(
                                    hintText: "Password",
                                    customerName: textEditingControllerPassword,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: screenWidth(context,dividedBy: 12),left:screenWidth(context,dividedBy: 15),top: screenHeight(context,dividedBy: 60)),
                                    child: Container(
                                      height: screenHeight(context,dividedBy: 20),
                                      width: screenWidth(context,dividedBy: 2.7),
                                      child: RaisedButton(

                                        color: Constants.kitGradients[3],
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(15)),
                                        onPressed: () {
                                          setState(() {
                                            isLoading = true;
                                          });
                                          userBloc.login(loginRequest: LoginRequest(username: textEditingControllerUserId.text,password: textEditingControllerPassword.text));
                                          setState(() {
                                            print("Token :" + tok.toString());
                                          });

                                          },
                                        child:isLoading==false? Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: screenWidth(context, dividedBy: 150)),
                                          child: Container(
                                            width: screenWidth(context, dividedBy: 5),
                                            child: Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  "Login",
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: Constants.kitGradients[1]),
                                                )),
                                          ),
                                        ):
                                            Padding(
                                              padding: EdgeInsets.symmetric(vertical: screenHeight(context,dividedBy: 300)),
                                              child: Center(child:
                                                CircularProgressIndicator(
                                                  valueColor:  new AlwaysStoppedAnimation<Color>(Colors.white),

                                                ),),
                                            )
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ),
                    ),


                  ],
                ),

                Positioned(
                  top: screenHeight(context,dividedBy: 1.2),
                  left: screenWidth(context,dividedBy: 1.5),
                  child: Container(
                    width: screenWidth(context,dividedBy: 4),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Powered by",style: TextStyle(fontSize: 15,color: Colors.black,fontFamily: 'Segoe',fontWeight: FontWeight.w300)
                        ),
                        Text("WeCodeLife",style: TextStyle(fontSize: 15,color: Colors.red[800],fontFamily: 'Segoe',fontWeight:FontWeight.w300),

                        ),

                      ],
                    ),
                  ),
                ),




                Positioned(
                    top: screenHeight(context,dividedBy: 1.16),
                    right: screenWidth(context,dividedBy: 1.5),
                    child: SvgPicture.asset("assets/icons/Ellipse.svg",),),

                Positioned(
                  top: screenHeight(context,dividedBy: 1.15),
                  left: screenWidth(context,dividedBy: 2.7),
                  child: ClipOval(
                    child:Container(
                      height: screenHeight(context,dividedBy: 11),
                      width: screenWidth(context,dividedBy: 4),
                      color: Constants.kitGradients[3],
                    )

                  ),
                ),
                Positioned(
                  top: screenHeight(context,dividedBy: 1.05),
                  left: screenWidth(context,dividedBy: 1.5),
                  child: ClipOval(
                      child:Container(
                        height: screenHeight(context,dividedBy: 19),
                        width: screenWidth(context,dividedBy: 14),
                        color: Constants.kitGradients[3],
                      )

                  ),
                )
    ],
            ),
          ),
        ],
      ),
    );
  }
}
