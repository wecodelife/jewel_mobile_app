import 'package:flutter/material.dart';
import 'package:jewel_app/src/bloc/user_bloc.dart';
import 'package:jewel_app/src/models/user_details_response.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/utils.dart';
import 'package:jewel_app/ui/widgets/cust_info_tile.dart';
import 'package:jewel_app/ui/widgets/drawer.dart';
import 'package:jewel_app/ui/widgets/search_icons.dart';
import 'dart:convert' as convert;

class CustomerInfo extends StatefulWidget {
  @override
  _CustomerInfoState createState() => _CustomerInfoState();
}

class _CustomerInfoState extends State<CustomerInfo> {
  List<String> searchCustSuggestionList = [];
  bool isLoading = false;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey<ScaffoldState>();
  UserBloc userBloc = UserBloc();
  TextEditingController custSearchTextEditingController =
      new TextEditingController();

  @override
  void initState() {
    userBloc.custDetails(searchKey: "");
    bool searchTap = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
      drawer: DrawerPage(),
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: screenHeight(
                      context,
                      dividedBy: 20,
                    ),
                    left: screenWidth(context, dividedBy: 30),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      _drawerKey.currentState.openDrawer();
                    },
                    child: Container(
                      height: screenHeight(
                        context,
                        dividedBy: 17,
                      ),
                      width: screenWidth(context, dividedBy: 11),
                      child: Icon(Icons.sort, color: Colors.black, size: 40),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(
                        context,
                        dividedBy: 25,
                      ),
                      left: screenWidth(context, dividedBy: 12)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 15),
                    width: screenWidth(context, dividedBy: 1.2),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black54),
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(30),
                        ),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(-0.4, -0.4),
                            color: Color(0xFFDBE7EB),
                            blurRadius: 3,
                            spreadRadius: 3,
                          ),
                          BoxShadow(
                            offset: Offset(0.4, 0.4),
                            color: Color(0xFFDBE7EB),
                            blurRadius: 3,
                            spreadRadius: 3,
                          )
                        ]),
                    child: TextField(
                      controller: custSearchTextEditingController,
                      onChanged: (_) {
                        userBloc.custDetails(
                            searchKey: custSearchTextEditingController.text);
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 80),
                            left: screenWidth(context, dividedBy: 15),
                          ),
                          hintText: "Search here",
                          hintStyle: TextStyle(
                              color: Colors.grey,
                              fontFamily: 'Poppins',
                              fontSize: 16),
                          suffixIcon: Padding(
                            padding: EdgeInsets.only(
                              right: screenWidth(context, dividedBy: 30),
                            ),
                            child: Icon(Search.search, color: Colors.black54),
                          ),
                          border: InputBorder.none),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 50)),
                  child: StreamBuilder<CustomerDetailsResponse>(
                      stream: userBloc.custDetailsResponse,
                      builder: (context, snapshot) {
                        return snapshot.hasData && snapshot.data.data.length > 0
                            ? ListView.builder(
                                itemCount: snapshot.data.data.length,
                                padding: EdgeInsets.all(0),
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (Buildcontext, int index) {
                                  print(snapshot
                                      .data.data[index].customer.houseName);
                                  String ph = "";
                                  int custId = 0;
                                  custId =
                                      snapshot.data.data[index].customer.id;
                                  print("length" +
                                      snapshot.data.data[index].customer.number
                                          .length
                                          .toString());
                                  if (snapshot.data.data[index].customer.number
                                          .length >
                                      0) {
                                    ph = snapshot.data.data[index].customer
                                        .number[0].phoneNumber
                                        .toString();
                                    print("phone" +
                                        snapshot.data.data[index].customer
                                            .number[0].phoneNumber
                                            .toString());
                                  }

                                  return GestureDetector(
                                    onTap: () {},
                                    child: Column(
                                      children: [
                                        CustomerInfoTile(
                                          houseNO: snapshot.data.data[index]
                                                  .customer.houseName +
                                              " " +
                                              snapshot.data.data[index].customer
                                                  .areaName,
                                          name: snapshot.data.data[index]
                                              .customer.brideName,
                                          city: snapshot.data.data[index]
                                                  .customer.place +
                                              " " +
                                              snapshot.data.data[index].customer
                                                  .district,
                                          phoneNum: ph,
                                          customerId: custId,
                                          bride: snapshot.data.data[index]
                                              .customer.brideName,
                                          guardian: snapshot.data.data[index]
                                              .customer.nameOfGuardian,
                                          place: snapshot
                                              .data.data[index].customer.place,
                                          house: snapshot.data.data[index]
                                              .customer.houseName,
                                          area: snapshot
                                              .data.data[index].customer.area,
                                          areaName: snapshot.data.data[index]
                                              .customer.areaName,
                                          district: snapshot.data.data[index]
                                              .customer.district,
                                          agent: snapshot
                                              .data.data[index].customer.agent
                                              .toString(),
                                          marriageDate: snapshot.data
                                              .data[index].customer.marriageDate
                                              .toString(),
                                          ph1: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  1
                                              ? snapshot
                                                  .data
                                                  .data[index]
                                                  .customer
                                                  .number[0]
                                                  .phoneNumber
                                                  .toString()
                                              : '',
                                          ph2: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  2
                                              ? snapshot
                                                  .data
                                                  .data[index]
                                                  .customer
                                                  .number[1]
                                                  .phoneNumber
                                                  .toString()
                                              : '',
                                          ph3: snapshot.data.data[index]
                                                      .customer.number.length ==
                                                  3
                                              ? snapshot
                                                  .data
                                                  .data[index]
                                                  .customer
                                                  .number[2]
                                                  .phoneNumber
                                                  .toString()
                                              : '',
                                          phStatus: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  1
                                              ? snapshot.data.data[index]
                                                  .customer.number[0].status
                                                  .toString()
                                              : '',
                                          phCustomer: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  1
                                              ? snapshot.data.data[index]
                                                  .customer.number[0].customer
                                                  .toString()
                                              : '',
                                          phId: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  1
                                              ? snapshot.data.data[index]
                                                  .customer.number[0].id
                                                  .toString()
                                              : '',
                                          phStatus2: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  2
                                              ? snapshot.data.data[index]
                                                  .customer.number[1].status
                                                  .toString()
                                              : '',
                                          phCustomer2: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  2
                                              ? snapshot.data.data[index]
                                                  .customer.number[1].customer
                                                  .toString()
                                              : '',
                                          phId2: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  2
                                              ? snapshot.data.data[index]
                                                  .customer.number[1].id
                                                  .toString()
                                              : '',
                                          phStatus3: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  3
                                              ? snapshot.data.data[index]
                                                  .customer.number[2].status
                                                  .toString()
                                              : '',
                                          phCustomer3: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  3
                                              ? snapshot.data.data[index]
                                                  .customer.number[2].customer
                                                  .toString()
                                              : '',
                                          phId3: snapshot.data.data[index]
                                                      .customer.number.length >=
                                                  3
                                              ? snapshot.data.data[index]
                                                  .customer.number[2].id
                                                  .toString()
                                              : '',
                                          // remarks: snapshot.data.data[index].customer.remarks[index].remarks,
                                        ),
                                      ],
                                    ),
                                  );
                                })
                            : snapshot.hasData && snapshot.data.data.length == 0
                                ? Container(
                                    height: screenHeight(context, dividedBy: 1),
                                    width: screenWidth(context, dividedBy: 1),
                                    child:
                                        Center(child: Text("No data Found!")),
                                  )
                                : Container(
                                    height: screenHeight(context, dividedBy: 1),
                                    width: screenWidth(context, dividedBy: 1),
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                Colors.red[800]),
                                      ),
                                    ));
                      }),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
