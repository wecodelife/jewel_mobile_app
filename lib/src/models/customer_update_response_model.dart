// To parse this JSON data, do
//
//     final customerUpdateNumberResponse = customerUpdateNumberResponseFromJson(jsonString);

import 'dart:convert';

CustomerUpdateNumberResponse customerUpdateNumberResponseFromJson(String str) => CustomerUpdateNumberResponse.fromJson(json.decode(str));

String customerUpdateNumberResponseToJson(CustomerUpdateNumberResponse data) => json.encode(data.toJson());

class CustomerUpdateNumberResponse {
  CustomerUpdateNumberResponse({
    this.message,
    this.status,
  });

  final String message;
  final int status;

  factory CustomerUpdateNumberResponse.fromJson(Map<String, dynamic> json) => CustomerUpdateNumberResponse(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
  };
}
