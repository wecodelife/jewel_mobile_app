// To parse this JSON data, do
//
//     final feildReportResponse = feildReportResponseFromJson(jsonString);

import 'dart:convert';

FeildReportResponse feildReportResponseFromJson(String str) => FeildReportResponse.fromJson(json.decode(str));

String feildReportResponseToJson(FeildReportResponse data) => json.encode(data.toJson());

class FeildReportResponse {
  FeildReportResponse({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final Data data;
  final int status;

  factory FeildReportResponse.fromJson(Map<String, dynamic> json) => FeildReportResponse(
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
    "status": status == null ? null : status,
  };
}

class Data {
  Data({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.marriageDate,
    this.financialBg,
    this.goldAmt,
    this.marriageSet,
    this.isExchanging,
    this.isExchangingOld,
    this.goldAmtExchanging,
    this.isTakingEmi,
    this.durationOfLoan,
    this.numberOfEmi,
    this.isAdvanceBooking,
    this.tokenAdvance,
    this.rateAtBooking,
    this.image1,
    this.image2,
    this.image3,
    this.isAnotherJewellerCame,
    this.isAdvancedInOther,
    this.amountAdvancedInOther,
    this.knowAnyNewParty,
    this.noOfParty,
    this.lat,
    this.lon,
    this.isAssigned,
    this.customer,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final dynamic marriageDate;
  final int financialBg;
  final dynamic goldAmt;
  final dynamic marriageSet;
  final bool isExchanging;
  final bool isExchangingOld;
  final String goldAmtExchanging;
  final bool isTakingEmi;
  final int durationOfLoan;
  final int numberOfEmi;
  final bool isAdvanceBooking;
  final dynamic tokenAdvance;
  final dynamic rateAtBooking;
  final dynamic image1;
  final dynamic image2;
  final dynamic image3;
  final bool isAnotherJewellerCame;
  final bool isAdvancedInOther;
  final dynamic amountAdvancedInOther;
  final bool knowAnyNewParty;
  final int noOfParty;
  final dynamic lat;
  final dynamic lon;
  final bool isAssigned;
  final int customer;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    marriageDate: json["marriage_date"],
    financialBg: json["financial_bg"] == null ? null : json["financial_bg"],
    goldAmt: json["gold_amt"],
    marriageSet: json["marriage_set"],
    isExchanging: json["is_exchanging"] == null ? null : json["is_exchanging"],
    isExchangingOld: json["is_exchanging_old"] == null ? null : json["is_exchanging_old"],
    goldAmtExchanging: json["gold_amt_exchanging"] == null ? null : json["gold_amt_exchanging"],
    isTakingEmi: json["is_taking_emi"] == null ? null : json["is_taking_emi"],
    durationOfLoan: json["duration_of_loan"] == null ? null : json["duration_of_loan"],
    numberOfEmi: json["number_of_emi"] == null ? null : json["number_of_emi"],
    isAdvanceBooking: json["is_advance_booking"] == null ? null : json["is_advance_booking"],
    tokenAdvance: json["token_advance"],
    rateAtBooking: json["rate_at_booking"],
    image1: json["image_1"],
    image2: json["image_2"],
    image3: json["image_3"],
    isAnotherJewellerCame: json["is_another_jeweller_came"] == null ? null : json["is_another_jeweller_came"],
    isAdvancedInOther: json["is_advanced_in_other"] == null ? null : json["is_advanced_in_other"],
    amountAdvancedInOther: json["amount_advanced_in_other"],
    knowAnyNewParty: json["know_any_new_party"] == null ? null : json["know_any_new_party"],
    noOfParty: json["no_of_party"] == null ? null : json["no_of_party"],
    lat: json["lat"],
    lon: json["lon"],
    isAssigned: json["is_assigned"] == null ? null : json["is_assigned"],
    customer: json["customer"] == null ? null : json["customer"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "marriage_date": marriageDate,
    "financial_bg": financialBg == null ? null : financialBg,
    "gold_amt": goldAmt,
    "marriage_set": marriageSet,
    "is_exchanging": isExchanging == null ? null : isExchanging,
    "is_exchanging_old": isExchangingOld == null ? null : isExchangingOld,
    "gold_amt_exchanging": goldAmtExchanging == null ? null : goldAmtExchanging,
    "is_taking_emi": isTakingEmi == null ? null : isTakingEmi,
    "duration_of_loan": durationOfLoan == null ? null : durationOfLoan,
    "number_of_emi": numberOfEmi == null ? null : numberOfEmi,
    "is_advance_booking": isAdvanceBooking == null ? null : isAdvanceBooking,
    "token_advance": tokenAdvance,
    "rate_at_booking": rateAtBooking,
    "image_1": image1,
    "image_2": image2,
    "image_3": image3,
    "is_another_jeweller_came": isAnotherJewellerCame == null ? null : isAnotherJewellerCame,
    "is_advanced_in_other": isAdvancedInOther == null ? null : isAdvancedInOther,
    "amount_advanced_in_other": amountAdvancedInOther,
    "know_any_new_party": knowAnyNewParty == null ? null : knowAnyNewParty,
    "no_of_party": noOfParty == null ? null : noOfParty,
    "lat": lat,
    "lon": lon,
    "is_assigned": isAssigned == null ? null : isAssigned,
    "customer": customer == null ? null : customer,
  };
}
