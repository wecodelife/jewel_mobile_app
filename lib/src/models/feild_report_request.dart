// To parse this JSON data, do
//
//     final feildReportRequest = feildReportRequestFromJson(jsonString);

import 'dart:convert';

FeildReportRequest feildReportRequestFromJson(String str) => FeildReportRequest.fromJson(json.decode(str));

String feildReportRequestToJson(FeildReportRequest data) => json.encode(data.toJson());

class FeildReportRequest {
  FeildReportRequest({
    this.marriageDate,
    this.financialBg,
    this.goldAmt,
    this.marriageSet,
    this.isExchanging,
    this.isExchangingOld,
    this.goldAmtExchanging,
    this.isTakingEmi,
    this.durationOfLoan,
    this.numberOfEmi,
    this.isAdvanceBooking,
    this.tokenAdvance,
    this.rateAtBooking,
    this.image1,
    this.image2,
    this.image3,
    this.isAnotherJewellerCame,
    this.isAdvancedInOther,
    this.amountAdvancedInOther,
    this.knowAnyNewParty,
    this.noOfParty,
    this.lat,
    this.lon,
    this.phoneNumber1,
    this.phoneNumber2,
    this.phoneNumber3,
    this.customer,
  });

  final dynamic marriageDate;
  final int financialBg;
  final dynamic goldAmt;
  final dynamic marriageSet;
  final bool isExchanging;
  final bool isExchangingOld;
  final dynamic goldAmtExchanging;
  final bool isTakingEmi;
  final int durationOfLoan;
  final int numberOfEmi;
  final bool isAdvanceBooking;
  final dynamic tokenAdvance;
  final dynamic rateAtBooking;
  final dynamic image1;
  final dynamic image2;
  final dynamic image3;
  final bool isAnotherJewellerCame;
  final bool isAdvancedInOther;
  final dynamic amountAdvancedInOther;
  final bool knowAnyNewParty;
  final int noOfParty;
  final dynamic lat;
  final dynamic lon;
  final dynamic phoneNumber1;
  final dynamic phoneNumber2;
  final dynamic phoneNumber3;
  final int customer;

  factory FeildReportRequest.fromJson(Map<String, dynamic> json) => FeildReportRequest(
    marriageDate: json["marriage_date"],
    financialBg: json["financial_bg"] == null ? null : json["financial_bg"],
    goldAmt: json["gold_amt"],
    marriageSet: json["marriage_set"],
    isExchanging: json["is_exchanging"] == null ? null : json["is_exchanging"],
    isExchangingOld: json["is_exchanging_old"] == null ? null : json["is_exchanging_old"],
    goldAmtExchanging: json["gold_amt_exchanging"],
    isTakingEmi: json["is_taking_emi"] == null ? null : json["is_taking_emi"],
    durationOfLoan: json["duration_of_loan"] == null ? null : json["duration_of_loan"],
    numberOfEmi: json["number_of_emi"] == null ? null : json["number_of_emi"],
    isAdvanceBooking: json["is_advance_booking"] == null ? null : json["is_advance_booking"],
    tokenAdvance: json["token_advance"],
    rateAtBooking: json["rate_at_booking"],
    image1: json["image_1"],
    image2: json["image_2"],
    image3: json["image_3"],
    isAnotherJewellerCame: json["is_another_jeweller_came"] == null ? null : json["is_another_jeweller_came"],
    isAdvancedInOther: json["is_advanced_in_other"] == null ? null : json["is_advanced_in_other"],
    amountAdvancedInOther: json["amount_advanced_in_other"],
    knowAnyNewParty: json["know_any_new_party"] == null ? null : json["know_any_new_party"],
    noOfParty: json["no_of_party"] == null ? null : json["no_of_party"],
    lat: json["lat"],
    lon: json["lon"],
    phoneNumber1: json["phone_number1"],
    phoneNumber2: json["phone_number2"],
    phoneNumber3: json["phone_number3"],
    customer: json["customer"] == null ? null : json["customer"],
  );

  Map<String, dynamic> toJson() => {
    "marriage_date": marriageDate,
    "financial_bg": financialBg == null ? null : financialBg,
    "gold_amt": goldAmt,
    "marriage_set": marriageSet,
    "is_exchanging": isExchanging == null ? null : isExchanging,
    "is_exchanging_old": isExchangingOld == null ? null : isExchangingOld,
    "gold_amt_exchanging": goldAmtExchanging,
    "is_taking_emi": isTakingEmi == null ? null : isTakingEmi,
    "duration_of_loan": durationOfLoan == null ? null : durationOfLoan,
    "number_of_emi": numberOfEmi == null ? null : numberOfEmi,
    "is_advance_booking": isAdvanceBooking == null ? null : isAdvanceBooking,
    "token_advance": tokenAdvance,
    "rate_at_booking": rateAtBooking,
    "image_1": image1,
    "image_2": image2,
    "image_3": image3,
    "is_another_jeweller_came": isAnotherJewellerCame == null ? null : isAnotherJewellerCame,
    "is_advanced_in_other": isAdvancedInOther == null ? null : isAdvancedInOther,
    "amount_advanced_in_other": amountAdvancedInOther,
    "know_any_new_party": knowAnyNewParty == null ? null : knowAnyNewParty,
    "no_of_party": noOfParty == null ? null : noOfParty,
    "lat": lat,
    "lon": lon,
    "phone_number1": phoneNumber1,
    "phone_number2": phoneNumber2,
    "phone_number3": phoneNumber3,
    "customer": customer == null ? null : customer,
  };
}
