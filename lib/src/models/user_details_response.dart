// To parse this JSON data, do
//
//     final customerDetailsResponse = customerDetailsResponseFromJson(jsonString);

import 'dart:convert';

CustomerDetailsResponse customerDetailsResponseFromJson(String str) => CustomerDetailsResponse.fromJson(json.decode(str));

String customerDetailsResponseToJson(CustomerDetailsResponse data) => json.encode(data.toJson());

class CustomerDetailsResponse {
  CustomerDetailsResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory CustomerDetailsResponse.fromJson(Map<String, dynamic> json) => CustomerDetailsResponse(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.user,
    this.customer,
  });

  final int user;
  final Customer customer;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    user: json["user"] == null ? null : json["user"],
    customer: json["customer"] == null ? null : Customer.fromJson(json["customer"]),
  );

  Map<String, dynamic> toJson() => {
    "user": user == null ? null : user,
    "customer": customer == null ? null : customer.toJson(),
  };
}

class Customer {
  Customer({
    this.id,
    this.brideName,
    this.nameOfGuardian,
    this.houseName,
    this.place,
    this.area,
    this.areaName,
    this.postOffice,
    this.district,
    this.marriageDate,
    this.number,
    this.remarks,
    this.agent
  });

  final int id;
  final String brideName;
  final String nameOfGuardian;
  final String houseName;
  final String place;
  final int area;
  final String areaName;
  final String postOffice;
  final String district;
  final DateTime marriageDate;
  final List<Number> number;
  final List<Number> remarks;
  final int agent;

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    id: json["id"] == null ? null : json["id"],
    brideName: json["bride_name"] == null ? null : json["bride_name"],
    nameOfGuardian: json["name_of_guardian"] == null ? null : json["name_of_guardian"],
    houseName: json["house_name"] == null ? null : json["house_name"],
    place: json["place"] == null ? null : json["place"],
    area: json["area"] == null ? null : json["area"],
    areaName: json["area_name"] == null ? null : json["area_name"],
    postOffice: json["post_office"] == null ? null : json["post_office"],
    district: json["district"] == null ? null : json["district"],
    marriageDate: json["marriage_date"] == null ? null : DateTime.parse(json["marriage_date"]),
    number: json["number"] == null ? null : List<Number>.from(json["number"].map((x) => Number.fromJson(x))),
    remarks: json["remarks"] == null ? null : List<Number>.from(json["remarks"].map((x) => Number.fromJson(x))),
    agent: json["agent"] == null ? null : json["agent"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "bride_name": brideName == null ? null : brideName,
    "name_of_guardian": nameOfGuardian == null ? null : nameOfGuardian,
    "house_name": houseName == null ? null : houseName,
    "place": place == null ? null : place,
    "area": area == null ? null : area,
    "area_name": areaName == null ? null : areaName,
    "post_office": postOffice == null ? null : postOffice,
    "district": district == null ? null : district,
    "marriage_date": marriageDate == null ? null : "${marriageDate.year.toString().padLeft(4, '0')}-${marriageDate.month.toString().padLeft(2, '0')}-${marriageDate.day.toString().padLeft(2, '0')}",
    "number": number == null ? null : List<dynamic>.from(number.map((x) => x.toJson())),
    "remarks": remarks == null ? null : List<dynamic>.from(remarks.map((x) => x.toJson())),
    "agent": agent == null ? null : agent,
  };
}

class Number {
  Number({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.phoneNumber,
    this.customer,
    this.status,
    this.remarks,
    this.date,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int phoneNumber;
  final int customer;
  final int status;
  final String remarks;
  final DateTime date;

  factory Number.fromJson(Map<String, dynamic> json) => Number(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    customer: json["customer"] == null ? null : json["customer"],
    status: json["status"] == null ? null : json["status"],
    remarks: json["remarks"] == null ? null : json["remarks"],
    date: json["date"] == null ? null : DateTime.parse(json["date"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "customer": customer == null ? null : customer,
    "status": status == null ? null : status,
    "remarks": remarks == null ? null : remarks,
    "date": date == null ? null : date.toIso8601String(),
  };
}
