// To parse this JSON data, do
//
//     final customerUpdateNumber = customerUpdateNumberFromJson(jsonString);

import 'dart:convert';

CustomerUpdateNumber customerUpdateNumberFromJson(String str) => CustomerUpdateNumber.fromJson(json.decode(str));

String customerUpdateNumberToJson(CustomerUpdateNumber data) => json.encode(data.toJson());

class CustomerUpdateNumber {
  CustomerUpdateNumber({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.phoneNumber,
    this.customer,
    this.status,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int phoneNumber;
  final int customer;
  final int status;

  factory CustomerUpdateNumber.fromJson(Map<String, dynamic> json) => CustomerUpdateNumber(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    customer: json["customer"] == null ? null : json["customer"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "customer": customer == null ? null : customer,
    "status": status == null ? null : status,
  };
}
