// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.token,
    this.user,
  });

  final String token;
  final User user;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    token: json["token"] == null ? null : json["token"],
    user: json["user"] == null ? null : User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token == null ? null : token,
    "user": user == null ? null : user.toJson(),
  };
}

class User {
  User({
    this.id,
    this.name,
    this.username,
    this.email,
    this.phoneNumber,
    this.key,
    this.userType,
    this.userTypeName,
    this.isAdmin,
    this.branch,
    this.branchName,
    this.company,
    this.companyName,
  });

  final int id;
  final String name;
  final String username;
  final dynamic email;
  final String phoneNumber;
  final String key;
  final int userType;
  final String userTypeName;
  final bool isAdmin;
  final int branch;
  final String branchName;
  final int company;
  final String companyName;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    username: json["username"] == null ? null : json["username"],
    email: json["email"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    key: json["key"] == null ? null : json["key"],
    userType: json["user_type"] == null ? null : json["user_type"],
    userTypeName: json["user_type_name"] == null ? null : json["user_type_name"],
    isAdmin: json["is_admin"] == null ? null : json["is_admin"],
    branch: json["branch"] == null ? null : json["branch"],
    branchName: json["branch_name"] == null ? null : json["branch_name"],
    company: json["company"] == null ? null : json["company"],
    companyName: json["company_name"] == null ? null : json["company_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "username": username == null ? null : username,
    "email": email,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "key": key == null ? null : key,
    "user_type": userType == null ? null : userType,
    "user_type_name": userTypeName == null ? null : userTypeName,
    "is_admin": isAdmin == null ? null : isAdmin,
    "branch": branch == null ? null : branch,
    "branch_name": branchName == null ? null : branchName,
    "company": company == null ? null : company,
    "company_name": companyName == null ? null : companyName,
  };
}
