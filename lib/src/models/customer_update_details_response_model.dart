// To parse this JSON data, do
//
//     final customerUpdateDetailsResponseModel = customerUpdateDetailsResponseModelFromJson(jsonString);

import 'dart:convert';

CustomerUpdateDetailsResponseModel customerUpdateDetailsResponseModelFromJson(String str) => CustomerUpdateDetailsResponseModel.fromJson(json.decode(str));

String customerUpdateDetailsResponseModelToJson(CustomerUpdateDetailsResponseModel data) => json.encode(data.toJson());

class CustomerUpdateDetailsResponseModel {
  CustomerUpdateDetailsResponseModel({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory CustomerUpdateDetailsResponseModel.fromJson(Map<String, dynamic> json) => CustomerUpdateDetailsResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.brideName,
    this.nameOfGuardian,
    this.houseName,
    this.place,
    this.postOffice,
    this.district,
    this.marriageDate,
    this.isAttended,
    this.isAssigned,
    this.area,
    this.agent,

  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String brideName;
  final String nameOfGuardian;
  final String houseName;
  final String place;
  final String postOffice;
  final String district;
  final String marriageDate;
  final bool isAttended;
  final bool isAssigned;
  final int area;
  final int agent;



  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    brideName: json["bride_name"] == null ? null : json["bride_name"],
    nameOfGuardian: json["name_of_guardian"] == null ? null : json["name_of_guardian"],
    houseName: json["house_name"] == null ? null : json["house_name"],
    place: json["place"] == null ? null : json["place"],
    postOffice: json["post_office"] == null ? null : json["post_office"],
    district: json["district"] == null ? null : json["district"],
    marriageDate: json["marriage_date"] == null ? null : json["marriage_date"],
    isAttended: json["is_attended"] == null ? null : json["is_attended"],
    isAssigned: json["is_assigned"] == null ? null : json["is_assigned"],
    area: json["area"] == null ? null : json["area"],
    agent: json["agent"] == null ? null : json["agent"],

  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "bride_name": brideName == null ? null : brideName,
    "name_of_guardian": nameOfGuardian == null ? null : nameOfGuardian,
    "house_name": houseName == null ? null : houseName,
    "place": place == null ? null : place,
    "post_office": postOffice == null ? null : postOffice,
    "district": district == null ? null : district,
    "marriage_date": marriageDate == null ? null : marriageDate,
    "is_attended": isAttended == null ? null : isAttended,
    "is_assigned": isAssigned == null ? null : isAssigned,
    "area": area == null ? null : area,
    "agent": agent == null ? null : agent,
  };
}
