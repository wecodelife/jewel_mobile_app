import 'package:jewel_app/src/models/customer_update_details.dart';
import 'package:jewel_app/src/models/customer_update_details_response_model.dart';
import 'package:jewel_app/src/models/customer_update_number.dart';
import 'package:jewel_app/src/models/customer_update_response_model.dart';
import 'package:jewel_app/src/models/destination_list_response.dart';
import 'package:jewel_app/src/models/feild_report_request.dart';
import 'package:jewel_app/src/models/feild_report_response.dart';
import 'package:jewel_app/src/models/login_request.dart';
import 'package:jewel_app/src/models/login_response.dart';
import 'package:jewel_app/src/models/logout_response.dart';
import 'package:jewel_app/src/models/notification_list_response.dart';
import 'package:jewel_app/src/models/state.dart';
import 'package:jewel_app/src/models/user_details_response.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:quiver/collection.dart';

class UserApiProvider {
  Future<State> sampleCall() async {
    final response = await ObjectFactory().apiClient.sampleApiCall();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<SampleResponseModel>.success(
          SampleResponseModel.fromJson(response.data));
    } else {
      return null;
    }
  }

  Future<State> loginCall(LoginRequest loginRequest) async {
    final response = await ObjectFactory().apiClient.loginRequest(loginRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<LoginResponse>.success(
          LoginResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> custDetailsCall(String searchKey) async {
    final response = await ObjectFactory()
        .apiClient
        .getCustomerDetails(searchKey: searchKey);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<CustomerDetailsResponse>.success(
          CustomerDetailsResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> feildReportCall(FeildReportRequest feildReportRequest) async {
    final response =
        await ObjectFactory().apiClient.feildReportRequest(feildReportRequest);
    print((response.toString()));
    if (response.statusCode == 200 || response.statusCode == 201 || true) {
      return State<FeildReportResponse>.success(
          FeildReportResponse.fromJson(response.data));
    } else {
      print("not found");
      return null;
    }
  }

  Future<State> updateCustomerDetails(
      CustomerUpdatedDetails customerUpdatedDetails, String id) async {
    final response = await ObjectFactory()
        .apiClient
        .updateCustomer(customerUpdatedDetails, id);

    print((response.toString()));
    if (response.statusCode == 200 || response.statusCode == 201 || true) {
      return State<CustomerUpdateDetailsResponseModel>.success(
          CustomerUpdateDetailsResponseModel.fromJson(response.data));
    } else {
      print("not found");
      return null;
    }
  }

  Future<State> customerLogout() async {
    final response = await ObjectFactory().apiClient.customerLogout();
    print((response.toString()));
    if (response.statusCode == 200) {
      ObjectFactory().appHive.deleteToken();
      return State<LogoutResponse>.success(
          LogoutResponse.fromJson(response.data));
    } else {
      print("not found");
      return null;
    }
  }

  Future<State> updateNumber(
      CustomerUpdateNumber customerUpdateNumber, String id) async {
    final response = await ObjectFactory()
        .apiClient
        .updatePhoneNumber(customerUpdateNumber, id);
    print((response.toString()));
    if (response.statusCode == 200) {
      return State<CustomerUpdateNumberResponse>.success(
          CustomerUpdateNumberResponse.fromJson(response.data));
    } else {
      print("not found");
      return null;
    }
  }
}
