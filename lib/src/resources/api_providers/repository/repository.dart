import 'package:jewel_app/src/models/customer_update_details.dart';
import 'package:jewel_app/src/models/customer_update_number.dart';
import 'package:jewel_app/src/models/feild_report_request.dart';
import 'package:jewel_app/src/models/login_request.dart';
import 'package:jewel_app/src/models/state.dart';
import 'package:jewel_app/src/models/user_details_response.dart';
import 'package:jewel_app/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> sampleCall() => userApiProvider.sampleCall();

  Future<State> login({LoginRequest loginRequest}) =>
      userApiProvider.loginCall(loginRequest);
  Future<State> getCustomerDetails({String searchKey}) =>
      userApiProvider.custDetailsCall(searchKey);
  Future<State> feildReport({FeildReportRequest feildReportRequest}) =>
      userApiProvider.feildReportCall(feildReportRequest);
  Future<State> updateCustomer(
          {CustomerUpdatedDetails customerUpdatedDetails, String id}) =>
      userApiProvider.updateCustomerDetails(customerUpdatedDetails, id);
  Future<State> updateNumber(
          {CustomerUpdateNumber customerUpdateNumber, String id}) =>
      userApiProvider.updateNumber(customerUpdateNumber, id);
  Future<State> customerLogout() => userApiProvider.customerLogout();
}
