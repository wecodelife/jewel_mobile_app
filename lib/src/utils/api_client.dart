import 'dart:async';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/material.dart';
import 'package:jewel_app/src/models/customer_update_details.dart';
import 'package:jewel_app/src/models/customer_update_details_response_model.dart';
import 'package:jewel_app/src/models/customer_update_number.dart';
import 'package:jewel_app/src/models/feild_report_request.dart';
import 'package:jewel_app/src/models/login_request.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/urls.dart';

class ApiClient {
  ApiClient() {
    initClient();
  }

//for api client testing only
  ApiClient.test({@required this.dio});

  Dio dio;
  BaseOptions _baseOptions;

  initClient() async {
    _baseOptions = new BaseOptions(
        baseUrl: Constants.baseUrl,
        connectTimeout: 30000,
        receiveTimeout: 1000000,
        followRedirects: true,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.acceptHeader: 'application/json',
        },
        responseType: ResponseType.json,
        receiveDataWhenStatusError: true);

    dio = Dio(_baseOptions);

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) {
        return true;
      };
    };

    dio.interceptors.add(CookieManager(new CookieJar()));
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions reqOptions) {
        return reqOptions;
      },
      onError: (DioError dioError) {
        return dioError.response;
      },
    ));
  }

  /// Verify User
  Future<Response> sampleApiCall() {
    return dio.post("", data: {});
  }

  /// feature list
  Future<Response> loginRequest(LoginRequest loginRequest) {
    FormData formData = FormData.fromMap(
        {"username": loginRequest.username, "password": loginRequest.password});
    return dio.post(Urls.loginUrl, data: formData);
  }

  Future<Response> feildReportRequest(
      FeildReportRequest feildReportRequest) async {
    // print(feildReportRequest.financialBg);
    // print(feildReportRequest.marriageDate);
    // print(feildReportRequest.amountAdvancedInOther);
    // print(feildReportRequest.customer);
    // print(feildReportRequest.durationOfLoan);
    // print(feildReportRequest.goldAmt);
    // print(feildReportRequest.image1);
    // print(feildReportRequest.image2);
    // print(feildReportRequest.image3);
    // print(feildReportRequest.knowAnyNewParty);
    // print(feildReportRequest.marriageSet);
    // print(feildReportRequest.noOfParty);
    // print(feildReportRequest.numberOfEmi);
    // print(feildReportRequest.rateAtBooking);
    // print(feildReportRequest.tokenAdvance);
    // print(feildReportRequest.isAdvanceBooking);
    // print(feildReportRequest.isAdvancedInOther);
    // print(feildReportRequest.isTakingEmi);
    // print(feildReportRequest.isExchangingOld);
    // print(feildReportRequest.isExchanging);
    // print(feildReportRequest.isAnotherJewellerCame);
print("number"+feildReportRequest.toString());
    FormData formDataFeildReport = FormData.fromMap({
      // if(feildReportRequest.financialBg!=null)
      "financial_bg": feildReportRequest.financialBg,
      // if(feildReportRequest.marriageDate!=null)
      "marriage_date": feildReportRequest.marriageDate,
      // if(feildReportRequest.amountAdvancedInOther!=null)
      "amount_advanced_in_other": feildReportRequest.amountAdvancedInOther,
      // if(feildReportRequest.customer!=null)
      "customer": feildReportRequest.customer,
      // if(feildReportRequest.durationOfLoan!=null)
      "duration_of_loan": feildReportRequest.durationOfLoan,
      // if(feildReportRequest.goldAmt!=null)
      "gold_amt": feildReportRequest.goldAmt,
      if(feildReportRequest.image1!=null)
      "image_1": await MultipartFile.fromFile(feildReportRequest.image1.path,
          filename: feildReportRequest.image1.path.split('/').last),
      if (feildReportRequest.image2 != null)
        "image_2": await MultipartFile.fromFile(feildReportRequest.image2.path,
            filename: feildReportRequest.image2.path.split('/').last),
      if (feildReportRequest.image3 != null)
        "image_3": await MultipartFile.fromFile(feildReportRequest.image3.path,
            filename: feildReportRequest.image3.path.split('/').last),
      if (feildReportRequest.knowAnyNewParty != null)
        "know_any_new_party": feildReportRequest.knowAnyNewParty,
      if (feildReportRequest.marriageSet != null)
        "marriage_set": feildReportRequest.marriageSet,
      if (feildReportRequest.noOfParty != null)
        "no_of_party": feildReportRequest.noOfParty,
      if (feildReportRequest.numberOfEmi != null)
        "number_of_emi": feildReportRequest.noOfParty,
      if (feildReportRequest.rateAtBooking != null)
        "rate_at_booking": feildReportRequest.rateAtBooking,
      if (feildReportRequest.tokenAdvance != null)
        "token_advance": feildReportRequest.tokenAdvance,
      if (feildReportRequest.isAdvanceBooking != null)
        "is_advance_booking": feildReportRequest.isAdvanceBooking,
      if (feildReportRequest.isAdvancedInOther != null)
        "is_advanced_in_other": feildReportRequest.isAdvancedInOther,
      if (feildReportRequest.isAnotherJewellerCame != null)
        "is_another_jeweller_came": feildReportRequest.isAnotherJewellerCame,
      if (feildReportRequest.isExchanging != null)
        "is_exchanging": feildReportRequest.isExchanging,
      if (feildReportRequest.isExchangingOld != null)
        "is_exchanging_old": feildReportRequest.isExchangingOld,
      if (feildReportRequest.isTakingEmi != null)
        "is_taking_emi": feildReportRequest.isTakingEmi,
      "lat": feildReportRequest.lat,
      "lon": feildReportRequest.lon,
      "gold_amt_exchanging": feildReportRequest.goldAmtExchanging,
      "phone_number1": feildReportRequest.phoneNumber1,
      "phone_number2": feildReportRequest.phoneNumber2,
      "phone_number3": feildReportRequest.phoneNumber3,
    });

    return dio.post(Urls.feildReportUrl, data: formDataFeildReport);
  }

  /// feature list

  /// search Flatmate
//  Future<Response> searchFlatmate(String searchText) {
//    dio.options.headers
//        .addAll({"Authorization": ObjectFactory().prefs.getAuthToken()});
//    return dio.get(
//        Urls.SEARCH_FLATMATE + "{\"search_param\"= \"$searchText\"}");
//  }

  Future<Response> getCustomerDetails({String searchKey}) {
    print("id"+ObjectFactory().appHive.getToken());
    print("id2"+ObjectFactory().appHive.hiveGet(key: "token"));
    dio.options.headers.addAll(
        {"Authorization": "Token " + ObjectFactory().appHive.getToken()});
    return dio.get(Urls.custDetailsUrl + "?q=$searchKey");
  }

  Future<Response> customerLogout() {
    dio.options.headers.addAll(
        {"Authorization": "Token " + ObjectFactory().appHive.getToken()});
    return dio.get(Urls.logoutUrl);
  }

  Future<Response> updatePhoneNumber(
      CustomerUpdateNumber customerUpdateNumber, String id) {
    print('update' + customerUpdateNumber.phoneNumber.toString());
    print('id' + id.toString());
    print('cust' + customerUpdateNumber.customer.toString());
    print('stat' + customerUpdateNumber.status.toString());
    dio.options.headers.addAll(
        {"Authorization": "Token " + ObjectFactory().appHive.getToken()});
    var data = {
      "id": customerUpdateNumber.id.toString(),
      "phone_number": customerUpdateNumber.phoneNumber.toString(),
      "customer": customerUpdateNumber.customer.toString(),
      "status": customerUpdateNumber.status.toString(),
    };
    return dio.put(Urls.updatePhoneNumber + "$id/", data: data);
  }

  Future<Response> updateCustomer(
      CustomerUpdatedDetails customerUpdatedDetails, String id) {
    print("number" + customerUpdatedDetails.toString());
    dio.options.headers.addAll(
        {"Authorization": "Token " + ObjectFactory().appHive.getToken()});
    var data = {
      if (customerUpdatedDetails.brideName != null)
        "bride_name": customerUpdatedDetails.brideName,
      if (customerUpdatedDetails.nameOfGuardian != null)
        "name_of_guardian": customerUpdatedDetails.nameOfGuardian,
      if (customerUpdatedDetails.marriageDate != null)
        "marriage_date": customerUpdatedDetails.marriageDate.split(" ").first,
      if (customerUpdatedDetails.place != null)
        "place": customerUpdatedDetails.place,
      if (customerUpdatedDetails.postOffice != null)
        "post_office": customerUpdatedDetails.postOffice,
      if (customerUpdatedDetails.area != null)
        "area": customerUpdatedDetails.area,
      if (customerUpdatedDetails.houseName != null)
        "house_name": customerUpdatedDetails.houseName,
      if (customerUpdatedDetails.district != null)
        "district": customerUpdatedDetails.district,
      if (customerUpdatedDetails.agent != null)
        "agent": customerUpdatedDetails.agent,
      //   "phone_numbers": customerUpdatedDetails.phoneNumbers,
    };
    return dio.put(Urls.custUpdateDetails + "$id/", data: data);
  }
}
