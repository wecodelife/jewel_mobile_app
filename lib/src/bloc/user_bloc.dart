import 'dart:async';
import 'package:jewel_app/src/models/customer_update_details.dart';
import 'package:jewel_app/src/models/customer_update_details_response_model.dart';
import 'package:jewel_app/src/models/customer_update_number.dart';
import 'package:jewel_app/src/models/customer_update_response_model.dart';
import 'package:jewel_app/src/models/feild_report_request.dart';
import 'package:jewel_app/src/models/feild_report_response.dart';
import 'package:jewel_app/src/models/login_request.dart';
import 'package:jewel_app/src/models/login_response.dart';
import 'package:jewel_app/src/models/logout_response.dart';
import 'package:jewel_app/src/models/notification_list_response.dart';
import 'package:jewel_app/src/models/state.dart';
import 'package:jewel_app/src/models/user_details_response.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<SampleResponseModel> _sample =
      new StreamController<SampleResponseModel>.broadcast();

  StreamController<LoginResponse> _login =
      new StreamController<LoginResponse>.broadcast();

  StreamController<CustomerDetailsResponse> _custDetails =
      new StreamController<CustomerDetailsResponse>.broadcast();

  StreamController<FeildReportResponse> _feildReport =
      new StreamController<FeildReportResponse>.broadcast();

  StreamController<CustomerUpdateDetailsResponseModel> _updateCustomer =
      new StreamController<CustomerUpdateDetailsResponseModel>.broadcast();

  StreamController<CustomerUpdateNumberResponse> _updateNumber =
      new StreamController<CustomerUpdateNumberResponse>.broadcast();

  StreamController<LogoutResponse> _customerLogout =
      new StreamController<LogoutResponse>.broadcast();

  //stream controller is broadcasting the  details

  Stream<SampleResponseModel> get sampleResponse => _sample.stream;

  Stream<LoginResponse> get loginResponse => _login.stream;

  Stream<CustomerDetailsResponse> get custDetailsResponse =>
      _custDetails.stream;

  Stream<FeildReportResponse> get addFieldReportResponse => _feildReport.stream;

  Stream<CustomerUpdateDetailsResponseModel> get updateCustomerResponse =>
      _updateCustomer.stream;

  Stream<CustomerUpdateNumberResponse> get updateNumberResponse =>
      _updateNumber.stream;

  Stream<LogoutResponse> get customerLogoutResponse => _customerLogout.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  sampleCall() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.sampleCall();

    if (state is SuccessState) {
      loadingSink.add(false);
      _sample.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _sample.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  login({LoginRequest loginRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.login(
        loginRequest: LoginRequest(
            username: loginRequest.username, password: loginRequest.password));

    if (state is SuccessState) {
      loadingSink.add(false);
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _login.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  custDetails({String searchKey}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .getCustomerDetails(searchKey: searchKey);

    if (state is SuccessState) {
      loadingSink.add(false);
      _custDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _custDetails.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  feildReport({FeildReportRequest feildReportRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.feildReport(
            feildReportRequest: FeildReportRequest(
          financialBg: feildReportRequest.financialBg,
          marriageDate: feildReportRequest.marriageDate,
          amountAdvancedInOther: feildReportRequest.amountAdvancedInOther,
          customer: feildReportRequest.customer,
          durationOfLoan: feildReportRequest.durationOfLoan,
          goldAmt: feildReportRequest.goldAmt,
          image1: feildReportRequest.image1,
          image2: feildReportRequest.image2,
          image3: feildReportRequest.image3,
          knowAnyNewParty: feildReportRequest.knowAnyNewParty,
          marriageSet: feildReportRequest.marriageSet,
          noOfParty: feildReportRequest.noOfParty,
          numberOfEmi: feildReportRequest.noOfParty,
          rateAtBooking: feildReportRequest.rateAtBooking,
          tokenAdvance: feildReportRequest.tokenAdvance,
          isAdvanceBooking: feildReportRequest.isAdvanceBooking,
          isAdvancedInOther: feildReportRequest.isAdvancedInOther,
          isAnotherJewellerCame: feildReportRequest.isAnotherJewellerCame,
          isExchanging: feildReportRequest.isExchanging,
          isExchangingOld: feildReportRequest.isExchanging,
          isTakingEmi: feildReportRequest.isTakingEmi,
          lat: feildReportRequest.lat,
          lon: feildReportRequest.lon,
          goldAmtExchanging: feildReportRequest.goldAmtExchanging,
          phoneNumber1: feildReportRequest.phoneNumber1,
          phoneNumber2: feildReportRequest.phoneNumber2,
          phoneNumber3: feildReportRequest.phoneNumber3,
        ));

    if (state is SuccessState) {
      loadingSink.add(false);
      _feildReport.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _feildReport.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  updateCustomer(
      {CustomerUpdatedDetails customerUpdatedDetails, String id}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.updateCustomer(
        customerUpdatedDetails: CustomerUpdatedDetails(
            area: customerUpdatedDetails.area,
            brideName: customerUpdatedDetails.brideName,
            district: customerUpdatedDetails.district,
            houseName: customerUpdatedDetails.houseName,
            marriageDate: customerUpdatedDetails.marriageDate,
            nameOfGuardian: customerUpdatedDetails.nameOfGuardian,
            place: customerUpdatedDetails.place,
            postOffice: customerUpdatedDetails.postOffice,
            agent: customerUpdatedDetails.agent),
        id: id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateCustomer.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateCustomer.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  updateNumber({CustomerUpdateNumber customerUpdateNumber, String id}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.updateNumber(
        customerUpdateNumber: CustomerUpdateNumber(
            id: customerUpdateNumber.id,
            customer: customerUpdateNumber.customer,
            phoneNumber: customerUpdateNumber.phoneNumber,
            status: customerUpdateNumber.status),
        id: id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateNumber.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateNumber.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  customerLogout() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.customerLogout();

    if (state is SuccessState) {
      loadingSink.add(false);
      _customerLogout.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _customerLogout.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _sample?.close();
    _login?.close();
    _custDetails?.close();
    _feildReport?.close();
    _customerLogout?.close();
    _updateCustomer?.close();
    _updateNumber?.close();
  }
}

UserBloc userBlocSingle = UserBloc();
