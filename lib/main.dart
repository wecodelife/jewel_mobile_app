import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:jewel_app/src/utils/constants.dart';
import 'package:jewel_app/src/utils/object_factory.dart';
import 'package:jewel_app/ui/screens/customer_info.dart';
import 'package:jewel_app/ui/screens/customer_update_page.dart';
import 'package:jewel_app/ui/screens/login_page.dart';
import 'package:jewel_app/ui/screens/report_page.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Hive.openBox(Constants.BOX_NAME);
  await Hive.openBox("agentName");
  Map<int, Color> color = {
    50: Color.fromRGBO(255, 92, 87, .1),
    100: Color.fromRGBO(255, 92, 87, .2),
    200: Color.fromRGBO(255, 92, 87, .3),
    300: Color.fromRGBO(255, 92, 87, .4),
    400: Color.fromRGBO(255, 92, 87, .5),
    500: Color.fromRGBO(255, 92, 87, .6),
    600: Color.fromRGBO(255, 92, 87, .7),
    700: Color.fromRGBO(255, 92, 87, .8),
    800: Color.fromRGBO(255, 92, 87, .9),
    900: Color.fromRGBO(255, 92, 87, 1),
  };  MaterialColor colorCustom = MaterialColor(0xFFAE1B38, color);


  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch:colorCustom ,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: ObjectFactory().appHive.getToken() == "" ||
          ObjectFactory().appHive.getToken() == null
          ? Login()
          : CustomerInfo()));

}

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         title: 'Flutter Demo',
//         theme: ThemeData(
//           // This is the theme of your application.
//           //
//           // Try running your application with "flutter run". You'll see the
//           // application has a blue toolbar. Then, without quitting the app, try
//           // changing the primarySwatch below to Colors.green and then invoke
//           // "hot reload" (press "r" in the console where you ran "flutter run",
//           // or simply save your changes to "hot reload" in a Flutter IDE).
//           // Notice that the counter didn't reset back to zero; the application
//           // is not restarted.
//           primarySwatch:colorCustom ,
//           // This makes the visual density adapt to the platform that you run
//           // the app on. For desktop platforms, the controls will be smaller and
//           // closer together (more dense) than on mobile platforms.
//           visualDensity: VisualDensity.adaptivePlatformDensity,
//         ),
//         home: ObjectFactory().appHive.getToken() == "" ||
//                 ObjectFactory().appHive.getToken() == null
//             ? Login()
//             : CustomerInfo());
//   }
// }
